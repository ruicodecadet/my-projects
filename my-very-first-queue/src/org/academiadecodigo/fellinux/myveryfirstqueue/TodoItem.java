package org.academiadecodigo.fellinux.myveryfirstqueue;

public class TodoItem implements Comparable<TodoItem> {

    private Importance importance;
    private int priority;
    private String text;

    public TodoItem(Importance importance, int priority, String text) {
        this.importance = importance;
        this.priority = priority;
        this.text = text;
    }

    @Override
    public int compareTo(TodoItem o) {
        if(this.importance.getOrder() == o.importance.getOrder() && this.priority == o.priority){
            return 0;
        }
        if (this.importance.getOrder() == o.importance.getOrder() && this.priority < o.priority){
            return 0;
        }
        if (this.importance.getOrder() > o.importance.getOrder()){
            return -1;
        }
        return 1;
    }

    public enum Importance {
        HIGH(1),
        MEDIUM(2),
        LOW(3);

        private int order;

        Importance(int order){
            this.order = order;
        }

        public int getOrder() {
            return order;
        }
    }

    @Override
    public String toString() {

        return "TodoItem{importance=" + importance +
                ", priority=" + priority +
                ", item=" + text +
                '}';
    }
}
