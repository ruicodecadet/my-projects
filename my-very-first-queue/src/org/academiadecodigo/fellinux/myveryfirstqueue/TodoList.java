package org.academiadecodigo.fellinux.myveryfirstqueue;

import java.util.PriorityQueue;

public class TodoList {

   	private PriorityQueue<TodoItem> priorityQueue = new PriorityQueue<>();

   /*	public TodoList() {
   	    this.priorityQueue = new PriorityQueue();
    }*/

    public void add(TodoItem item) {
        priorityQueue.add(item);
    }

    public String remove() {
        return priorityQueue.remove().toString();
    }

    public boolean isEmpty(){
        return priorityQueue.isEmpty();
    }

}