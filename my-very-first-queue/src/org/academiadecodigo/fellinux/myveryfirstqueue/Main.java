package org.academiadecodigo.fellinux.myveryfirstqueue;

import java.util.Iterator;

public class Main {
    public static void main(String[] args) {

        TodoList todoList = new TodoList();

        todoList.add(new TodoItem(TodoItem.Importance.MEDIUM, 1, "Comer bem"));
        todoList.add(new TodoItem(TodoItem.Importance.LOW, 1, "Dormir bem"));
        todoList.add(new TodoItem(TodoItem.Importance.HIGH, 1, "Ter saúde"));
        todoList.add(new TodoItem(TodoItem.Importance.LOW, 2, "Dormir assim-assim"));
        todoList.add(new TodoItem(TodoItem.Importance.MEDIUM, 2, "Comer assim-assim"));
        todoList.add(new TodoItem(TodoItem.Importance.HIGH, 2, "Ter saúde assim-assim"));

        while (!todoList.isEmpty()) {
            System.out.println(todoList.remove());
        }
    }
}
