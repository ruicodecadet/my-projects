package org.academiadecodigo.bootcamp.gfx.simplegfx;

import org.academiadecodigo.bootcamp.grid.GridColor;
import org.academiadecodigo.bootcamp.grid.GridDirection;
import org.academiadecodigo.bootcamp.grid.position.AbstractGridPosition;
import org.academiadecodigo.bootcamp.grid.position.GridPosition;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

/**
 * Simple graphics position
 */
public class SimpleGfxGridPosition extends AbstractGridPosition {

    private Rectangle rectangle;
    private SimpleGfxGrid simpleGfxGrid;

    /**
     * Simple graphics position constructor
     * @param grid Simple graphics grid
     */
    public SimpleGfxGridPosition(SimpleGfxGrid grid){
        super((int) (Math.random() * grid.getCols()), (int) (Math.random() * grid.getRows()), grid);

        simpleGfxGrid = (SimpleGfxGrid) getGrid();

        int x = simpleGfxGrid.columnToX(super.getCol());
        int y = simpleGfxGrid.rowToY(super.getRow());

        rectangle = new Rectangle(x, y, simpleGfxGrid.getCellSize(), simpleGfxGrid.getCellSize());

        show();
        //throw new UnsupportedOperationException();
    }

    /**
     * Simple graphics position constructor
     * @param col position column
     * @param row position row
     * @param grid Simple graphics grid
     */
    public SimpleGfxGridPosition(int col, int row, SimpleGfxGrid grid){
        super(col, row, grid);

        simpleGfxGrid = grid;

        int x = simpleGfxGrid.columnToX(col);
        int y = simpleGfxGrid.rowToY(row);

        rectangle = new Rectangle(x, y, simpleGfxGrid.getCellSize(), simpleGfxGrid.getCellSize());

        show();
        //throw new UnsupportedOperationException();
    }

    /**
     * @see GridPosition#show()
     */
    @Override
    public void show() {
        //throw new UnsupportedOperationException();
        this.rectangle.fill();
    }

    /**
     * @see GridPosition#hide()
     */
    @Override
    public void hide() {
        this.rectangle.delete();
        //throw new UnsupportedOperationException();
    }

    /**
     * @see GridPosition#moveInDirection(GridDirection, int)
     */
    @Override
    public void moveInDirection(GridDirection direction, int distance) {

        int previousCol = super.getCol();
        int previousRow = super.getRow();

        super.moveInDirection(direction, distance);

        int horizontalDistance = simpleGfxGrid.columnToX(super.getCol()) - simpleGfxGrid.columnToX(previousCol);
        int verticalDistance = simpleGfxGrid.rowToY(super.getRow()) - simpleGfxGrid.rowToY(previousRow);

        this.rectangle.translate(horizontalDistance, verticalDistance);

        //System.out.println(verticalDistance);
        //System.out.println(horizontalDistance);

        /*verticalDistance += super.getRow() - previousRow;
        horizontalDistance += super.getCol() - previousCol;*/

        //System.out.println(verticalDistance);
        //System.out.println(horizontalDistance);

        //System.out.println(direction);
        //System.out.println(distance);
        //System.out.println(super.getCol());
        //System.out.println(super.getRow());

        /*switch (direction) {

            case UP:
                rectangle.translate(0, (previousRow <10 ? 0 : verticalDistance * SimpleGfxGrid.CELL_SIZE));
                //System.out.println((super.getRow() - distance) * SimpleGfxGrid.CELL_SIZE);
                //moveUp(distance);
                break;
            case DOWN:
                rectangle.translate(0, (previousRow> 240 ? 0 : verticalDistance * SimpleGfxGrid.CELL_SIZE));
                //System.out.println((super.getRow() + distance) * SimpleGfxGrid.CELL_SIZE);
                //moveDown(distance);
                break;
            case LEFT:
                rectangle.translate(previousCol < 0 ? 0 : (horizontalDistance * SimpleGfxGrid.CELL_SIZE), 0);
                //System.out.println((super.getCol() - distance) * SimpleGfxGrid.CELL_SIZE);
                //moveLeft(distance);
                break;
            case RIGHT:
                rectangle.translate(previousCol > 800 ? 0 : (horizontalDistance * SimpleGfxGrid.CELL_SIZE), 0);
                //System.out.println((super.getCol() - distance) * SimpleGfxGrid.CELL_SIZE);
                //moveRight(distance);
                break;
        }*/

        //throw new UnsupportedOperationException();
    }

    /**
     * @see AbstractGridPosition#setColor(GridColor)
     */
    @Override
    public void setColor(GridColor color) {
        //Implement

        this.rectangle.setColor(SimpleGfxColorMapper.getColor(color));
        super.setColor(color);

        /*switch (color) {
            case RED:
                rectangle.setColor(Color.RED);
                super.setColor(GridColor.RED);
                break;
            case GREEN:
                rectangle.setColor(Color.GREEN);
                super.setColor(GridColor.GREEN);
                break;
            case BLUE:
                rectangle.setColor(Color.BLUE);
                super.setColor(GridColor.BLUE);
                break;
            case MAGENTA:
                rectangle.setColor(Color.MAGENTA);
                super.setColor(GridColor.MAGENTA);
                break;
            default:
                break;
        }*/

        //throw new UnsupportedOperationException();
    }
}
