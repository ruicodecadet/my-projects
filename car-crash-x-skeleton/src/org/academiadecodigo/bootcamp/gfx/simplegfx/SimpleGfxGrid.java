package org.academiadecodigo.bootcamp.gfx.simplegfx;

import org.academiadecodigo.bootcamp.grid.Grid;
import org.academiadecodigo.bootcamp.grid.position.AbstractGridPosition;
import org.academiadecodigo.bootcamp.grid.position.GridPosition;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import java.awt.*;

public class SimpleGfxGrid implements Grid {

    public static final int PADDING = 10;
    public static final int CELL_SIZE = 15;
    private  Rectangle rectangle;
    private int cols;
    private int rows;



    public SimpleGfxGrid(int cols, int rows){
        this.cols = cols;
        this.rows = rows;
    }

    /**
     * @see Grid#init()
     */
    @Override
    public void init() {
        //throw new UnsupportedOperationException();

        this.rectangle = new Rectangle (PADDING, PADDING, cols * CELL_SIZE, rows * CELL_SIZE); //(getX(), getY(), getWidth(), getHeight());
        this.rectangle.draw();

    }

    /**
     * @see Grid#getCols()
     */
    @Override
    public int getCols() {
        //Implement
        return this.cols;
        //throw new UnsupportedOperationException();
    }

    /**
     * @see Grid#getRows()
     */
    @Override
    public int getRows() {
        //Implement
        return this.rows;
        //throw new UnsupportedOperationException();
    }

    /**
     * Obtains the width of the grid in pixels
     * @return the width of the grid
     */
    public int getWidth() {
        //Implement
        return cols * CELL_SIZE;
        //throw new UnsupportedOperationException();
    }

    /**
     * Obtains the height of the grid in pixels
     * @return the height of the grid
     */
    public int getHeight() {
        //Implement
        return rows * CELL_SIZE;
        //throw new UnsupportedOperationException();
    }

    /**
     * Obtains the grid X position in the SimpleGFX canvas
     * @return the x position of the grid
     */
    public int getX() {
        //Implement
        return this.rectangle.getX(); //PADDING;
        //throw new UnsupportedOperationException();
    }

    /**
     * Obtains the grid Y position in the SimpleGFX canvas
     * @return the y position of the grid
     */
    public int getY() {
        //Implement
        return this.rectangle.getY(); //PADDING;
        //throw new UnsupportedOperationException();
    }

    /**
     * Obtains the pixel width and height of a grid position
     * @return
     */
    public int getCellSize() {
        //Implement
        return this.CELL_SIZE;
        //throw new UnsupportedOperationException();
    }

    /**
     * @see Grid#makeGridPosition()
     */
    @Override
    public GridPosition makeGridPosition() {
        //This is tied to CarFactory - create and place car

        return new SimpleGfxGridPosition(this);

        //throw new UnsupportedOperationException();
    }

    /**
     * @see Grid#makeGridPosition(int, int)
     */
    @Override
    public GridPosition makeGridPosition(int col, int row) {
        //Used only in test?

        return new SimpleGfxGridPosition(col, row, this);

        //throw new UnsupportedOperationException();
    }

    /**
     * Auxiliary method to compute the y value that corresponds to a specific row
     * @param row index
     * @return y pixel value
     */
    public int rowToY(int row) {
        //Implement
        return PADDING + CELL_SIZE * row;
        //throw new UnsupportedOperationException();
    }

    /**
     * Auxiliary method to compute the x value that corresponds to a specific column
     * @param column index
     * @return x pixel value
     */
    public int columnToX(int column) {
        //Implement
        return PADDING + CELL_SIZE * column;
        //throw new UnsupportedOperationException();
    }
}
