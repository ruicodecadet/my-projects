public class Player {
    private String name;
    private int playerGuess;

    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int pickANumber (int min, int max){
        playerGuess = Randomizer.randombetween(min, max);
        return playerGuess;
    }


}
