package org.academiadecodigo.fellinux.uniquewords;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class UniqueWord implements Iterable<String> {

    /*private String text;
    private String[] word;*/

    private Set<String> words;

    public UniqueWord(String text) {
        this.words = new HashSet<>();
        this.add(text);
        //word = text.split(" ");
    }

    public void add(String text) {
        for (String word : text.split(" ")) {
            words.add(word);
        }
    }

    @Override
    public Iterator<String> iterator() {
        return words.iterator();
    }

}
