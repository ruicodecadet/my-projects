package org.academiadecodigo.felinux.explicit;

public class ThreadLauncher {

    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {
            Thread thread = new Thread(new MySpecialThread());
            thread.setName("Thread-" + i);
            thread.start();
        }
    }
}
