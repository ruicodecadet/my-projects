package org.academiadecodigo.felinux.implicit;

import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

public class Alarm {

    private Timer timer;
    private Ring ring;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Number of times to ring? ");
        int numRings = Integer.parseInt(scanner.next());

        System.out.println("Ring interval in seconds? ");
        int ringInterval = Integer.parseInt(scanner.next());

        System.out.println(Thread.currentThread().getName());
        Alarm alarm = new Alarm();
        Thread thread = new Thread();
        alarm.start(ringInterval, numRings);
        System.out.println(" Eu sou a main e já parei de executar");
    }

    private void start(int ringInterval, int numRings) {

        ring = new Ring(numRings);
        timer = new Timer();
        timer.scheduleAtFixedRate(ring, ringInterval * 1000, ringInterval* 1000);
    }

    private void stop(){
        timer.cancel();
    }

    private class Ring extends TimerTask {

        int numRings;

        public Ring(int numRings){
           this.numRings = numRings;
        }

        @Override
        public void run() {

            System.out.println(Thread.currentThread().getName());

            System.out.println("Alarm is ringing....");

           numRings--;

           if (numRings == 0){
               System.out.println("Alarm has stopped");
               stop();
           }
        }

    }
}
