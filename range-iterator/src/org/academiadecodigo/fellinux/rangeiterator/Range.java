package org.academiadecodigo.fellinux.rangeiterator;

import java.util.Iterator;

public class Range implements Iterable<Integer>{

    private int minRange;
    private int maxRange;

    public Range(int minRange, int maxRange) {
        this.minRange = minRange;
        this.maxRange = maxRange;
    }

    @Override
    public Iterator iterator() {
        return new RangeIterator();
    }

    private class RangeIterator implements Iterator<Integer> {

        private int currentNumber = minRange;

        @Override
        public boolean hasNext() {
            return currentNumber <= maxRange;
        }

        @Override
        public Integer next() {
            return currentNumber++;
            //return new RangeToIterate((int)(Math.random()*(maxRange - minRange) + minRange));
        }

    }
}
