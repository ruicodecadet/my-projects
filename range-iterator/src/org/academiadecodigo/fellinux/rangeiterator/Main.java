package org.academiadecodigo.fellinux.rangeiterator;

import java.util.Iterator;

public class Main {
    public static void main(String[] args) {

        Range range = new Range(10, 20);

        for (Integer number : range) {
            System.out.println("The number is " + number);
        }
    }
}
