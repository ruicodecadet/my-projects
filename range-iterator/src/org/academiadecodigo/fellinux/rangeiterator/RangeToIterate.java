package org.academiadecodigo.fellinux.rangeiterator;

public class RangeToIterate {

    private int number;

    public RangeToIterate(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

}
