package org.academiadecodigo.felinux.webserver.http;

import org.academiadecodigo.felinux.webserver.server.FileHandler;
import org.academiadecodigo.felinux.webserver.http.header.ContentType;
import org.academiadecodigo.felinux.webserver.http.header.ContentLength;
import org.academiadecodigo.felinux.webserver.http.header.StatusCode;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;

public class Response {

    public static final String GET = "GET";
    private static final String VERSION = "HTTP/1.0 ";
    private static final String NEW_LINE = "\r\n";

    private StatusCode statusCode;
    private ContentLength contentLength;
    private ContentType contentType;
    private File file;

    public void init(String method, String resource) {

        if (!method.equals(GET)) {
            statusCode = StatusCode.METHOD_NOT_ALLOWED;
            return;
        }

        file = FileHandler.find(resource);

        if (file == null) {
            statusCode = StatusCode.NOT_FOUND;
            return;
        }

        contentType = ContentType.get(file.getPath());

        if (contentType == null) {
            statusCode = StatusCode.UNSUPPORTED_MEDIA_TYPE;
            return;
        }

        statusCode = StatusCode.OK;
        contentLength = new ContentLength(file);
    }

    public void send(DataOutputStream output) throws IOException {

        output.writeBytes(VERSION);
        output.writeBytes(statusCode.build());
        output.writeBytes(NEW_LINE);

        if (statusCode == StatusCode.OK) {

            output.writeBytes(contentType.build());
            output.writeBytes(NEW_LINE);
            output.writeBytes(contentLength.build());
            output.writeBytes(NEW_LINE);

            output.writeBytes(NEW_LINE);
            FileHandler.copy(file, output);
        }

        output.flush();
    }

}
