package org.academiadecodigo.felinux.webserver.http.header;

public enum StatusCode implements Header {

    METHOD_NOT_ALLOWED(405, "Method Not Allowed"),
    NOT_FOUND(404, "Not Found"),
    OK(200, "Document Follows"),
    UNSUPPORTED_MEDIA_TYPE(415, "Unsupported Media Type");

    private int code;
    private String message;

    StatusCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String build() {

        switch (this) {

            case NOT_FOUND:
            case OK:
            case UNSUPPORTED_MEDIA_TYPE:
                return code + " " + message + ";";
            case METHOD_NOT_ALLOWED:
                return code + " " + message + "; Allow: GET";

        }

        throw new IllegalStateException();
    }
}
