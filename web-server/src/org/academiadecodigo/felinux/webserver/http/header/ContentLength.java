package org.academiadecodigo.felinux.webserver.http.header;

import java.io.File;

public class ContentLength implements Header {

    private File file;

    public ContentLength(File file) {
        this.file = file;
    }

    @Override
    public String build() {
        return "Content-Length: " + file.length();
    }
}
