package org.academiadecodigo.felinux.webserver.server;

import org.academiadecodigo.felinux.webserver.server.ClientHandler;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private static final int PORT = 8888;

    private ServerSocket socket;

    public Server() throws IOException {

        socket = new ServerSocket(PORT);
    }

    public void start() {

        while (true) {

            try {

                Socket client = this.socket.accept();
                new ClientHandler(client).run();

            } catch (IOException e) {

                System.err.println(e.getMessage());

            }
        }
    }
}
