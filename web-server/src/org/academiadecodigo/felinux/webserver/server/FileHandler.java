package org.academiadecodigo.felinux.webserver.server;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class FileHandler {

    private static final String ROOT = "/";
    private static final String WWW = "www";

    public static File find(String resource) {

        String path = resource.equals(ROOT) ? resource + "index.html" : resource;
        File file = new File(WWW + path);

        return file.exists() && !file.isDirectory() ? file : null;
    }

    public static void copy(File file, DataOutputStream output) throws IOException {
        Files.copy(file.toPath(), output);
    }
}
