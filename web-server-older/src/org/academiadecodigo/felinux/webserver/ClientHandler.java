package org.academiadecodigo.felinux.webserver;

import org.academiadecodigo.felinux.webserver.http.Request;
import org.academiadecodigo.felinux.webserver.http.Response;

import java.io.*;
import java.net.Socket;

public class ClientHandler implements Runnable {

    private Socket client;

    public ClientHandler(Socket client) {
        this.client = client;
    }

    @Override
    public void run() {

        try {

            Request request = new Request();
            request.receive(new BufferedReader(new InputStreamReader(client.getInputStream())));

            Response response = new Response();
            response.init(request.method(), request.resource());
            response.send(new DataOutputStream(client.getOutputStream()));

        } catch (IOException e) {
            System.err.println(e.getMessage());

        } finally {
            stop();
        }
    }

    private void stop() {

        try {
            client.close();

        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
