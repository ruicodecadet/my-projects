package org.academiadecodigo.felinux.webserver;

import javax.swing.plaf.TableHeaderUI;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private static final int PORT = 8888;

    private ServerSocket socket;

    public Server() throws IOException {

        socket = new ServerSocket(PORT);
    }

    public void start() {

        while (true) {

            try {

                Socket client = this.socket.accept();

                ClientHandler clientHandler = new ClientHandler(client);

                Thread clientThread = new Thread(clientHandler);
                clientThread.start();

            } catch (IOException e) {

                System.err.println(e.getMessage());

            }
        }
    }
}
