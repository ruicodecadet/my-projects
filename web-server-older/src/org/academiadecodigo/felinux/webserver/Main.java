package org.academiadecodigo.felinux.webserver;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        try {

            new Server().start();

        } catch (IOException e) {

            System.err.println(e.getMessage());
        }
    }
}
