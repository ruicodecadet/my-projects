package org.academiadecodigo.felinux.webserver.http.header;

public interface Header {

    String build();
}
