package org.academiadecodigo.felinux.webserver.http.header;

public enum ContentType implements Header {

    HTML("text"),
    PNG("image");

    private String type;

    ContentType(String type) {
        this.type = type;
    }

    public static ContentType get(String resource) {

        int dotIndex = resource.lastIndexOf(".");
        String extension = resource.substring(dotIndex + 1);

        for (ContentType contentType : values()) {

            if (contentType.name().toLowerCase().equals(extension)) {
                return contentType;
            }
        }

        return null;
    }

    @Override
    public String build() {

        StringBuilder builder = new StringBuilder("Content-Type: ")
                .append(type)
                .append("/")
                .append(name().toLowerCase());

        if (type.equals("text")) {
            builder.append("; charset=UTF-8");
        }

        return builder.toString();
    }
}
