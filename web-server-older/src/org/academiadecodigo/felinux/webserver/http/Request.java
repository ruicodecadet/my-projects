package org.academiadecodigo.felinux.webserver.http;

import java.io.BufferedReader;
import java.io.IOException;

public class Request {

    private static final String DELIMITER = " ";

    private String content;

    public void receive(BufferedReader reader) throws IOException {
        content = read(reader);
    }

    public String method() {
        return getWordAtIndex(0);
    }

    public String resource() {
        return getWordAtIndex(1);
    }

    private String read(BufferedReader reader) throws IOException {

        String line = reader.readLine();
        StringBuilder builder = new StringBuilder();

        while (line != null && !line.isEmpty()) {
            builder.append(line + "\n");
            line = reader.readLine();
        }

        return builder.toString();
    }

    private String getWordAtIndex(int index) {

        String[] words = content.split(DELIMITER);
        return words.length > index ? words[index] : null;
    }
}
