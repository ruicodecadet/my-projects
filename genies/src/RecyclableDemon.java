public class RecyclableDemon extends Genie{

    private boolean recyclable = true;

    public RecyclableDemon (String name, int maxWishes){
        super(name, maxWishes);
    }

    public boolean isRecyclable(boolean recyclable) {
        this.recyclable = recyclable;
        return recyclable;
    }

    public void recycle(){
        recyclable = false;
    }

    @Override
    public int grantWish(){
        if(recyclable == true){
            return 1;
        }
        return -1;
    }
}
