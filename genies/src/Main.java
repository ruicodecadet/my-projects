public class Main {
    public static void main(String[] args) {
        Genie[] genies = new Genie[100];
        MagicLamp ali = new MagicLamp(10, 5);
        MagicLamp abu = new MagicLamp(20, 10);

        //Genie alibaba = new Genie("Alibaba", 15);

        for(int i = 1; i < ali.getNumOfGenies(); i++){
            genies[i] = ali.rub("Alibaba" + i, 15);
            System.out.println(genies[i]);
        }

        genies[11] = abu.rub("Abulala", 1000);

        System.out.println("Bom dia!");
        System.out.println(genies[11]);

        System.out.println(abu.getRemainingGenies());
        System.out.println(abu.getNumOfGenies());

        System.out.println((genies[11].grantWish()) == 1? "Wish granted! Now go away...": "I only grant one wish, ok?!");
        System.out.println((genies[11].grantWish()) == 1? "Wish granted! Now go away...": "I only grant one wish, ok?!");

        System.out.println((genies[11].getWishesGranted()) + "Wish granted!");
        System.out.println((genies[11].getWishesGranted()) + "Wish granted!");

        System.out.println(genies[9]);
        System.out.println(genies[10]);


    }
}
