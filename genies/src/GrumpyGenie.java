public class GrumpyGenie extends Genie {

    private int maxWishesGG = 1;

    public GrumpyGenie(String name, int maxWishes){
        super(name, maxWishes);
    }

    @Override
    public int grantWish(){

        if(maxWishesGG > 0){
            maxWishesGG -= 1;
             return 1;
        }
        return -1;

    }
}
