public class MagicLamp {

    private int numOfGenies;
    private int remainingGenies;
    private int numOfRecharges;

    boolean recyclableDemon = true;

    public MagicLamp(int numOfGenies, int maxWishes){
        this.numOfGenies = numOfGenies;
        this.remainingGenies = numOfGenies;

        //RecyclableDemon recyclableDemon = new RecyclableDemon();
    }

    //Instead of RecyclableDemon use Genie because in Main it's not possible to know genie type
    public int recharge(Genie genie){
        RecyclableDemon demon = null;
        numOfRecharges = 0;
        if(demon.isRecyclable(recyclableDemon)){
            demon.recycle();
            recyclableDemon = false;
            numOfRecharges += 1;
            //Reset remaining genies to initial state
            remainingGenies = numOfGenies;
        }
        return numOfRecharges;
    }

    public Genie rub(String name, int wishes){
        if(remainingGenies >= 0){
            int chooseGenie = remainingGenies % 2;
            remainingGenies -= 1;
            return (chooseGenie == 0? new GrumpyGenie(name, wishes): new FriendlyGenie(name, wishes));
        }
        return new RecyclableDemon(name, wishes);
    }

    public boolean compare(MagicLamp lamp){
        return this.numOfGenies == lamp.numOfGenies && remainingGenies == lamp.remainingGenies && numOfRecharges == lamp.numOfRecharges;
        //((genies1 == genies2)&&(remaining1 == remaining2)&&(recharge1 == recharge2))
    }

    public int getNumOfGenies(){
        return numOfGenies;
    }

    public int getRemainingGenies(){
        return remainingGenies;
    }

    public int getNumOfRecharges(){
        return numOfRecharges;
    }
}
