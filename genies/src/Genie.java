import javax.swing.*;

public class Genie {

    private int maxWishes;
    private String name;
    private int wishesGranted;

    public Genie(String name, int maxWishes){
        this.name = name;
        this.maxWishes = maxWishes;
        this.wishesGranted = 0;
    }

    public int grantWish(){
        if(maxWishes > 0){
            maxWishes -= 1;
            wishesGranted++;
            return 1;
        }
        return -1;
    }

    public int getWishesGranted() {
        return wishesGranted;
    }
}
