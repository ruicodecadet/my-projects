package org.academiadecodigo.carcrash.field;

public class Position {

    private int col;
    private int row;

    public Position(int col, int row) {
        this.col = col;
        this.row = row;
    }

    //Criar metodo mover-se
    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

   public void moveRight() {
        if((this.col) < Field.getWidth() - 1) {
            this.col++;
            return;
        }
        this.col = Field.getWidth() - 1;
   }

   public void moveLeft() {
       if((this.col) > 0) {
           this.col--;
           return;
       }
       this.col =  0;
   }

   public void moveUp() {
       if((this.row) > 0) {
           this.row--;
           return;
       }
       this.row = 0;
   }

   public void moveDown() {
       if((this.row) < Field.getHeight() - 1) {
           this.row++;
           return;
       }
       this.row = Field.getHeight() - 1;
   }
}
