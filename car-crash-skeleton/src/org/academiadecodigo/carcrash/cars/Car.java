package org.academiadecodigo.carcrash.cars;

import com.sun.source.tree.PackageTree;
import org.academiadecodigo.carcrash.field.Position;

abstract public class Car {

    /** The position of the car on the grid */
    private Position pos;

    //Criar propriedade booleana para metodo isCrashed
    private boolean crashed;

    private CarType carType;

    private int move;

    public Position getPos() {
        return pos;
    }

    public void setPos(Position position) {
        this.pos = position;
    }

    public boolean isCrashed() {
        return crashed;
    }

    public void setCrashed(boolean crashed) {
        this.crashed = crashed;
    }

    public void crash() {
        crashed = true;
    }

    //abstract public void move();

    public void moveCar(){
        int possibleMoves = 4;
        //int actualCol = pos.getCol();
        //int actualRow = pos.getRow();

        int randomNum = (int) (Math.random() * possibleMoves);

        switch (randomNum){
            case 0:
                //if(!(pos.moveRight() == actualCol - 1)){
                    pos.moveRight();
                //}
                break;
            case 1:
                //if (!(moveLeft() == actualCol + 1)) {
                    pos.moveLeft();
                //}
                break;
            case 2:
                //if (!(moveDown() == actualRow + 1)) {
                    pos.moveDown();
                //}
                break;
            case 3:
                //if (!(moveUp() == actualRow - 1)){
                pos.moveUp();
                //}
                break;
            default:
                break;
        }
    }

    public int getMove() {
        return move;
    }

    public void setMove(int move) {
        this.move = move;
    }
}
