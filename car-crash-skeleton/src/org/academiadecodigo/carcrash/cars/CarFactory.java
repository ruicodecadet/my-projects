package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Field;
import org.academiadecodigo.carcrash.field.Position;

public class CarFactory {

    private static Position pos;
    //private Car newFiat, newMustang;

    //Substituir a implementacao null por produzir Fiat e Mustang
    public static  Car getNewCar() {

        int randomCol = (int) (Math.random() * Field.getWidth());
        int randomRow = (int) (Math.random() * Field.getHeight());

        pos = new Position(randomCol, randomRow);

        //Criar random para fazer carros diferentes
        int numOfCars = CarType.values().length;
        CarType[] typeOfCars = CarType.values();
        int randomCar = (int) (Math.random() * numOfCars);

        switch (typeOfCars[randomCar]) {
            case FIAT:
                Fiat newFiat = new Fiat(pos);
                return newFiat;
            case MUSTANG:
                Mustang newMustang = new Mustang(pos);
                return newMustang;
            default:
                return new Fiat(pos);
        }

    }

  /*  @Override
    public String toString() {
        return pos.toString();
    }*/
}
