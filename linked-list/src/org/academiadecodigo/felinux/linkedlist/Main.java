package org.academiadecodigo.felinux.linkedlist;

import java.util.Iterator;

public class Main {
    public static void main(String[] args) {

        LinkedList <String> list = new LinkedList();
        LinkedList <Integer> numList = new LinkedList();

        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        list.add("e");

        numList.add(1);
        numList.add(2);
        numList.add(3);
        numList.add(4);

        for (String element: list) {
            System.out.println(element);
        }

        for (Integer element: numList) {
            System.out.println(element);
        }

        Iterator<String> it = list.iterator();

        while (it.hasNext()) {

            String i = it.next();
            //System.out.println(i);
            if (i == "a") {
                it.remove();
            }
        }

        for (String element: list) {
            System.out.println(element);
        }

    }

}
