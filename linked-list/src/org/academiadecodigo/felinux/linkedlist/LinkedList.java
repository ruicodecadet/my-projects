package org.academiadecodigo.felinux.linkedlist;

import java.util.Iterator;

/**
 * A simple Linked List container implementation
 */
public class LinkedList<T> implements Iterable<T> {

    private int length;
    private Node head;

    public LinkedList() {
        this.length = 0;
        this.head = new Node(null);
    }

    /**
     * Obtain the number of data elements on the list
     *
     * @return the length of the list
     */
    int size() {
        return length;
    }

    /**
     * Adds a data element to the container
     *
     * @param data the element to add
     */
    void add(T data) {

        Node node = new Node(data);
        Node iterator = head;

        // Advance to the end of the list
        while (iterator.getNext() != null) {
            iterator = iterator.getNext();
        }

        iterator.setNext(node);
        length++;

    }

    /**
     * Returns the index of the first occurrence of the data element in the list
     *
     * @param data the element to search for
     * @return the index of the element, or -1 if the list does not contain the element
     */
    int indexOf(T data) {

        int index = 0;
        Node iterator = head.getNext();

        while (iterator != null) {

            if (iterator.getData().equals(data)) {
                return index;
            }

            iterator = iterator.getNext();
            index++;
        }

        // Data was not found
        return -1;

    }

    /**
     * Returns the data element at the specified index
     *
     * @param index index of the the element to return
     * @return the returned element or null if not found
     */
    public T get(int index) {

        Node iterator = head.getNext();

        while (iterator != null) {

            if (index == 0) {
                return iterator.getData();
            }

            iterator = iterator.getNext();
            index--;
        }

        // not found
        return null;

    }

    /**
     * Removes the first occurrence of the data element from the list
     *
     * @param data the element to remove
     * @return true if the list contains the element
     */
    boolean remove(T data) {

        Node previous = head;
        Node iterator = head.getNext();

        while (iterator != null) {

            if (iterator.getData().equals(data)) {

                previous.setNext(iterator.getNext());
                length--;
                return true;

            }

            previous = iterator;
            iterator = iterator.getNext();

        }

        return false;

    }

    /**
     * Linked List Node, used internally by the LinkedList class only
     */
    private class Node {

        /**
         * Reference to the next Node in the list
         */
        private Node next;

        /**
         * Reference to the data
         */
        private T data;

        Node(T data) {
            this.data = data;
            this.next = null;
        }

        void setNext(Node next) {
            this.next = next;
        }

        T getData() {
            return data;
        }

        Node getNext() {
            return next;
        }

    }

    @Override
    public Iterator iterator() {
        return new ListIterator();
    }

    private class ListIterator implements Iterator<T> {

        private Node current = head;

        @Override
        public boolean hasNext() {
            return (current.next != null);
        }

        @Override
        public T next() {
            current = current.next;
            return current.getData();

        }

        @Override
        public void remove() {
            LinkedList.this.remove(current.getData());
        }

    }

}