import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

public class HostNameResolver {

    //public String input;

    public static void main(String[] args) throws IOException {

    /*    String inputStream = null;
        assert inputStream != null;
        Scanner s = new Scanner(inputStream).useDelimiter("\\A");
        String result = s.hasNext() ? s.next() : "";

    }
    public void Scanner(InputStream source, String charsetName) {
        InputStream inputStream = null;
        Scanner s = new Scanner(inputStream);

    }*/
/*
    Scanner scan= new Scanner(System.in);

    //For string

    String text= scan.nextLine();

    System.out.println(text);

    //for int

    int num= scan.nextInt();

    System.out.println(num);*/
        String inputString = null;
        Scanner scanner = new Scanner(System.in);
        try {

            System.out.print("Enter a string : ");

            inputString = scanner.nextLine();
            InetAddress ipAddress = getHost(inputString);

            System.out.println("String read from console is : \n" + ipAddress);
            System.out.println(ipAddress.isReachable(1000) ? "Ok" : "FAIL");
        } catch (UnknownHostException e) {
            System.out.println("invalid host name: " + inputString);
        }

        finally {
            close(scanner);
        }


    }

    public static void close(Scanner scanner) {
        scanner.close();
    }

    public static InetAddress getHost(String string) throws UnknownHostException {
        InetAddress address = InetAddress.getByName(string);
        //System.out.println(address.getHostAddress());
        return address;
    }

}
