package org.academiadecodigo.bootcamp.concurrency;

import org.academiadecodigo.bootcamp.concurrency.bqueue.BQueue;

/**
 * Produces and stores integers into a blocking queue
 */
public class Producer implements Runnable {

    private final BQueue<Integer> queue;
    private int elementNum;

    /**
     * @param queue      the blocking queue to add elements to
     * @param elementNum the number of elements to produce
     */
    public Producer(BQueue queue, int elementNum) {
        this.queue = queue;
        this.elementNum = elementNum;

    }

    @Override
    public synchronized void run() {
        int test = 0;
        while (elementNum > 0) {

            test = ((int) Math.random() * queue.getLimit());

            synchronized (queue) {
                //System.out.println(queue.getLimit());
                //System.out.println("Inside Producer class!");

                System.out.println("Inside Producer class!");


                queue.offer(test);
                System.out.println(test);

                if (queue.getSize() == queue.getLimit()) {

                    System.out.println("Thread " + Thread.currentThread().getName() + " has left the building");

                }

                --elementNum;

                try {

                    Thread.sleep((int) Math.random() * 100);

                } catch (InterruptedException e) {

                    //System.out.println("Buffer full!");

                }

            }

        }

    }

}