package org.academiadecodigo.bootcamp.concurrency.bqueue;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Blocking Queue
 *
 * @param <T> the type of elements stored by this queue
 */
public class BQueue<T> {

    final private int limit;

    final private Queue<T> list;

    /**
     * Constructs a new queue with a maximum size
     *
     * @param limit the queue size
     */
    public BQueue(int limit) {

        this.limit = limit;
        list = new PriorityQueue(limit);
        //list = new LinkedList<>();

    }

    /**
     * Inserts the specified element into the queue
     * Blocking operation if the queue is full
     *
     * @param data the data to add to the queue
     */
    public void offer(T data) {

        synchronized (this) {

            while (list.size() == limit) {

                try {

                    wait();

                } catch (InterruptedException e) {

                }

            }


            list.offer(data);
            System.out.println("## ELEMENT ADDED, SIZE OF QUEUE IS NOW " + list.size() + " ##");
            notify();
            //throw new UnsupportedOperationException();
        }

    }

    /**
     * Retrieves and removes data from the head of the queue
     * Blocking operation if the queue is empty
     *
     * @return the data from the head of the queue
     */
    public T poll() {

        synchronized (this) {

            while (list.size() == 0) {

                try {

                    wait();

                } catch (InterruptedException e) {

                }

            }

            T value = list.poll();
            System.out.println("## ELEMENT REMOVED, SIZE OF QUEUE IS NOW " + list.size() + " ##");
            notifyAll();
            return value;

        }

    }
    //throw new UnsupportedOperationException();


    /**
     * Gets the number of elements in the queue
     *
     * @return the number of elements
     */
    public int getSize() {

        synchronized (this) {
            return list.size();
        }
        //throw new UnsupportedOperationException();

    }

    /**
     * Gets the maximum number of elements that can be present in the queue
     *
     * @return the maximum number of elements
     */
    public int getLimit() {

        return this.limit;

        //throw new UnsupportedOperationException();

    }

}
