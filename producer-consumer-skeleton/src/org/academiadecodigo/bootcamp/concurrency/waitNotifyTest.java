package org.academiadecodigo.bootcamp.concurrency;

public class waitNotifyTest {
    private static final long SLEEP_INTERVAL = 5000;
    private boolean running = true;
    private Thread thread;
    public void start() {
        print("Inside start() method");
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                print("Inside run() method");
                try {
                    System.out.println(SLEEP_INTERVAL);
                    Thread.sleep(SLEEP_INTERVAL);
                    System.out.println(SLEEP_INTERVAL);
                } catch(InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
                synchronized(waitNotifyTest.this) {
                    running = false;
                    waitNotifyTest.this.notify();
                }
            }
        });
        thread.start();
    }
    public void join() throws InterruptedException {
        print("Inside join() method");
        synchronized(this) {
            while(running) {
                print("Waiting for the peer thread to finish.");
                wait(); //waiting, not running
            }
            print("Peer thread finished.");
        }
    }
    private void print(String s) {
        System.out.println(s);
    }
    public static void main(String[] args) throws InterruptedException {
        waitNotifyTest test = new waitNotifyTest();
        test.start();
        test.join();
    }
}