package org.academiadecodigo.bootcamp.concurrency;

import org.academiadecodigo.bootcamp.concurrency.bqueue.BQueue;

/**
 * Consumer of integers from a blocking queue
 */
public class Consumer implements Runnable {

    private final BQueue<Integer> queue;
    private int elementNum;
    private boolean running;

    /**
     * @param queue the blocking queue to consume elements from
     * @param elementNum the number of elements to consume
     */
    public Consumer(BQueue queue, int elementNum) {
        this.queue = queue;
        this.elementNum = elementNum;
    }

    @Override
    public void run() {

        while (elementNum > 0) {

            synchronized (queue) {
                int elem = queue.poll();

                System.out.println("Thread " + Thread.currentThread().getName() + " has consumed element " + elem + " from the list");

                if (queue.getSize() == 0) {
                    System.out.println(Thread.currentThread().getName() + " has left the queue empty");

                }

            }

            --elementNum;

            System.out.println("Inside consumer class");
            try {

                Thread.sleep((int) Math.random() * 100);

            } catch (InterruptedException e) {

                System.out.println("Out of elements");

            }

        }

    }

}
