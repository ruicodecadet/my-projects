public class Player {

    private String name;

    //Método construtor dos jogadores
    public Player(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    //HandType[] result = HandType.values();
    public static int chooseAHand(HandType result){

        switch (result) {
            case PAPER:
                return 0;

            case ROCK:
                return 1;

            case SCISSORS:
                return 2;

            default:
                return -1;
        }
    }
}
