import org.w3c.dom.ls.LSOutput;

import java.awt.print.Paper;

public class Game {

    private Player[] players;
    private int maxRound;
    private HandType[] option = {HandType.PAPER, HandType.ROCK, HandType.SCISSORS};

    public Game(Player[] player) {
        this.players = player;
    }

    public void start(Player[] players) {

        //System.out.println(option);

        boolean endGame = false;
        maxRound = 0;

        while ((!endGame) && (maxRound <= 10)) {
        //for (int i = 0; i <= maxRound; i++){

            HandType indexPlayer1 = HandType.handPick(option);
            HandType indexPlayer2 = HandType.handPick(option);

            int playerBet1;
            playerBet1 = Player.chooseAHand(indexPlayer1);
            int playerBet2;
            playerBet2 = Player.chooseAHand(indexPlayer2);

            for (Player player : players) {

                if (playerBet1 == playerBet2) {
                    System.out.println("We have a draw");

                } else if (playerBet1 < playerBet2) {
                    System.out.println("Player " + player.getName() + " ganhou!");
                    endGame = true;
                    //break;
                }

            }
            maxRound += 1;

        }
    }



}
