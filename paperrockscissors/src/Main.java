public class Main {
    public static void main(String[] args) {
        Player[] players = new Player[]{
                new Player("Zeze"),
                new Player("Toni")
        };


        Game game = new Game(players);
        game.start(players);
    }
}
