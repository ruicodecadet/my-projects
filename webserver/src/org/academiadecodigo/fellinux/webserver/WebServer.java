package org.academiadecodigo.fellinux.webserver;

import com.sun.net.httpserver.HttpExchange;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;

public class WebServer {

    private static final int PORT = 9001;

    private Socket clientSocket;
    private static ServerSocket serverSocket;
    private static BufferedReader inputBufferedReader;
    private static BufferedOutputStream dataOut;

    public static void main(String[] args) throws IOException {

        WebServer webServer = new WebServer(PORT);

    }

    /*public abstract class HttpHandler {

    }*/

    public WebServer(int port) {

        try {

            // bind the socket to specified port
            System.out.println("Binding to port " + port);
            serverSocket = new ServerSocket(port);

            System.out.println("Server started: " + serverSocket);

            // block waiting for a client to connect
            System.out.println("Waiting for a client connection");
            clientSocket = serverSocket.accept();

            System.out.println("Client accepted: " + clientSocket);
            setupSocketStream();


        } catch (IOException ioe) {

            System.out.println(ioe.getMessage());

        }

    }

    private String setupSocketStream() throws IOException {

        String httpVerb = "";
        String serverDomain = "";
        String httpVersion = "";
        //String line;
        String firstLine;
        inputBufferedReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
/*
        while ((line = inputBufferedReader.readLine()) != null) {
            System.out.println(line);
        }
*/
        firstLine = inputBufferedReader.readLine();
        //System.out.println(firstLine);

        StringTokenizer st = new StringTokenizer(firstLine);
        while (st.hasMoreTokens()) {
            httpVerb = st.nextToken();
            serverDomain = st.nextToken();
            httpVersion = st.nextToken();

            //System.out.println(st.nextToken());

        }

        System.out.println(serverDomain);

        return serverDomain;
    }

   /* private String serverFeedback() {

    }*/

}
