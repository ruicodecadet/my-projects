package org.academiadecodigo.bootcamp.service;

import org.academiadecodigo.bootcamp.model.User;
import org.academiadecodigo.bootcamp.persistence.ConnectionManager;
import org.academiadecodigo.bootcamp.utils.Security;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JdbcUserService implements UserService {

    private ConnectionManager connectionManager;
    private Connection connection;

    public JdbcUserService() {
        this.connectionManager = new ConnectionManager();
        this.connection = connectionManager.getConnection();
    }

    @Override
    public boolean authenticate(String username, String password) {

        try {

            String query = "SELECT username, password FROM user WHERE username = ?;";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, username);

            ResultSet resultSet = statement.executeQuery();
            resultSet.next();

            if (username.equals(resultSet.getString("username")) && Security.getHash(password).equals(resultSet.getString("password"))) {
                System.out.println(connection);
                return true;

            }

        } catch (SQLException ex) {

            ex.printStackTrace();

        }

        return false;
    }

    @Override
    public void add(User user) {

        try {
            System.out.println(connection);
            // create a new statement
            Statement statement = connection.createStatement();

            // create a query
            String query = "INSERT INTO user (username, email, password, firstname, lastname, phone) VALUES ("
                    + "," + user.getUsername()
                    + "," + user.getEmail()
                    + "," + user.getPassword()
                    + "," + user.getFirstName()
                    + "," + user.getLastName()
                    + "," + user.getPhone()
                    + ");";

            statement.executeUpdate(query);

        } catch (SQLException sqlException) {

            System.out.println(sqlException.getMessage());

        }

    }

    @Override
    public User findByName(String username) {

        User user = null;

        try {

            java.sql.Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db?useTimezone=true&serverTimezone=UTC", "root", "root");

            // create a new statement
            Statement statement = connection.createStatement();

            // create a query
            String query = "SELECT * FROM user AS u WHERE u.username = " + username;

            // execute the query
            ResultSet resultSet = statement.executeQuery(query);

            // get the results
            if (resultSet.next()) {

                String user_name = resultSet.getString("username");
                String email = resultSet.getString("email");
                String password = resultSet.getString("password");
                String firstName = resultSet.getString("firstName2");
                String lastName = resultSet.getString("lastName");
                String phone = resultSet.getString("phone");

                user = new User(user_name, email, password, firstName, lastName, phone);
            }

        } catch (SQLException sqlException) {

            System.out.println(sqlException.getMessage());

        }

        return user;

    }

    @Override
    public List<User> findAll() {

        List<User> list = new ArrayList<>();

        try {

            // create a new statement
            Statement statement = connection.createStatement();

            // create a query
            String query = "SELECT * FROM user";

            // execute the query
            ResultSet resultSet = statement.executeQuery(query);

            // get the results

            while (resultSet.next()) {

                String username = resultSet.getString("username");
                String email = resultSet.getString("email");
                String password = resultSet.getString("password");
                String firstName = resultSet.getString("firstName2");
                String lastName = resultSet.getString("lastName");
                String phone = resultSet.getString("phone");

                list.add(new User(username, email, password, firstName, lastName, phone));

            }

        } catch (SQLException sqlException) {

            System.out.println(sqlException.getMessage());

        }

        return list;

    }

    @Override
    public int count() {

        int result = 0;

        try {

            // create a new statement
            Statement statement = connection.createStatement();

            // create a query
            String query = "SELECT COUNT(*) FROM user";

            // execute the query
            ResultSet resultSet = statement.executeQuery(query);

            // get the results
            if (resultSet.next()) {
                result = resultSet.getInt("username");
            }

        } catch (SQLException sqlException) {

            System.out.println(sqlException.getMessage());

        }

        return result;

    }

}
