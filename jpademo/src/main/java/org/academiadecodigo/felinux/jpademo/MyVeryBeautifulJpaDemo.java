package org.academiadecodigo.felinux.jpademo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class MyVeryBeautifulJpaDemo {

    public static void main(String[] args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpatest");

        EntityManager em = emf.createEntityManager();

        System.out.println("Result: " +
                em.createNativeQuery("SELECT 1 + 1").getSingleResult());

        em.close();

        emf.close();
    }
}
