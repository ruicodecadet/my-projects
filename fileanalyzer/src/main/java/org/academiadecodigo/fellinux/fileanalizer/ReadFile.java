package org.academiadecodigo.fellinux.fileanalizer;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

public class ReadFile {

    private String filePath;

    public ReadFile(String filePath) {
        this.filePath = filePath;
    }

    /*public readFile() {

        List<String> data = new LinkedList<String>();

        try {
            File myObj = new File(filePath);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                data = myReader.next();

                System.out.println(data);
            }

            return data;

            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }*/

    public String readLineOfText() {

        BufferedReader inputBufferedReader;
        String result = "";
        String line = "";

        try {

            inputBufferedReader = new BufferedReader(new FileReader(filePath));

            while((line = inputBufferedReader.readLine()) != null) {
                result += line;
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());

        }

        return result;
    }

    public Stream<String> streamCreator() {
        return Stream.of(readLineOfText().split(" "));
    }

    public void countWords() {
        System.out.println(streamCreator().count());
    }

}
