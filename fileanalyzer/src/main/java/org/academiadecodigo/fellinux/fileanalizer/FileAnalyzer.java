package org.academiadecodigo.fellinux.fileanalizer;

public class FileAnalyzer {

    private static final String FILE_PATH = "src/main/resources/text.txt";

    public static void main(String[] args) {

        ReadFile readFile = new ReadFile(FILE_PATH);

        for (String word : readFile.readLineOfText().split(" ")) {
            System.out.println(word);
        }

        readFile.countWords();

    }

}
