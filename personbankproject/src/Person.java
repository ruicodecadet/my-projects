public class Person {

    private String name;
    private Wallet wallet;
    private Bank bank;

    public Person(String name, Bank bank, Wallet wallet, double money) {
        this.name = name;
        this.bank = bank;
        this.wallet = wallet;

        wallet.receiveMoney(money);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double spendMoney(double money){
        return wallet.useMoney(money);
    }

    public double refillWallet(double money){

        //Outra alternativa
        //double moneyToRefill = bank.withdraw(money);
        //wallet.receiveMoney(moneyToRefill);
        return wallet.receiveMoney(bank.withdraw(money));
    }

    public double saveMoney(double money){

        //Outra alternativa
        //double moneyToSave = wallet.giveMoney(money);
        //bank.deposit(moneyToSave);
        return bank.deposit(wallet.giveMoney(money));
    }
    @Override
    public String toString(){
        return "Person{" + "name " + name + ", bank " + bank + ", wallet" + " money}";
    }
}
