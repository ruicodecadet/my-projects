public class Playground {
    public static void main(String[] args) {
        Wallet w1 = new Wallet();
        Wallet w2 = new Wallet();
        Bank bcp = new Bank("BCP");

        Person p1 = new Person("Catarina", bcp, w1, 200);
        System.out.println(p1);

        Person p2 = new Person("Toto", bcp, w2, 10);
        System.out.println(p2);

        p1.saveMoney(1000);
        System.out.println(p1);
        System.out.println(p2);

        p1.spendMoney(100);
        System.out.println(p1);
        System.out.println(p2);
    }
}
