public class Bank {

    private String name;
    private double bankMoney;

    public Bank(String name){
        this.name = name;
        this.bankMoney = 0;
    }

    public double getBankMoney(){
        return bankMoney;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double withdraw(double money){

        //Exception case handling
        if(money > this.bankMoney){
            double removedMoney = this.bankMoney;
            this.bankMoney = 0;
            return removedMoney;
        }

        //Normal case
        bankMoney -= money;
        return money;
    }

    public double deposit(double money){
        bankMoney += money;
        return bankMoney;
    }

}
