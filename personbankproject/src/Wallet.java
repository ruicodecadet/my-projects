public class Wallet {
    private double walletMoney;
    //private String wallet;

    //Metodo construtor de Wallet porque tem o nome da classe
    public Wallet(){
        walletMoney = 50;
    }

    public double receiveMoney(double money){

        walletMoney += money;
        return walletMoney;
    }

    public double giveMoney(double money){

        //Exception case handling
        if(money > this.walletMoney){
            double removedMoney = this.walletMoney;
            this.walletMoney = 0;
            return removedMoney;
        }

        //Normal case
        walletMoney -= money;
        return money;
    }

    public double useMoney(double money){

        //Exception case handling
        if(money > this.walletMoney){
            double removedMoney = this.walletMoney;
            this.walletMoney = 0;
            return removedMoney;
        }

        //Normal case
        walletMoney -= money;
        return money;
    }

}
