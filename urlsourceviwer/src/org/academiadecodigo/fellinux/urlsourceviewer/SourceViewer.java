package org.academiadecodigo.fellinux.urlsourceviewer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;

public class SourceViewer {

    //private Socket socket;
    private static BufferedReader inputBufferedReader;
    private static Scanner scanner;

    public static void getContent(String serverAddress) throws IOException {

        URL url = new URL(serverAddress);
        InputStream input = url.openStream();

        try {
            String line = "";
            inputBufferedReader = new BufferedReader(new InputStreamReader(input));
            while((line = inputBufferedReader.readLine()) != null) {
                System.out.println(line);
            }

        // } catch (UnknownHostException ex) {

        } catch (IOException ex) {

            ex.printStackTrace();

        } finally {

            input.close();

        }

    }

    public static void main(String[] args) throws IOException {

        getContent(readUrl());
        //System.out.println(readUrl());

    }

    public static String readUrl() {

        String line = "";
        scanner = new Scanner(System.in);

        System.out.println("Write your url: ");
        line = scanner.nextLine();

        return line;

    }

}
