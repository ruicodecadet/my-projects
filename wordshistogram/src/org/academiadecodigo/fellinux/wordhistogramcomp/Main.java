package org.academiadecodigo.fellinux.wordhistogramcomp;

public class Main {

    private static final String TEXT = "what is the madness is this madness what is life batata batata batata batata batatuxa";

    public static void main(String[] args) {

        WordHistogramComp wordHistogramComp = new WordHistogramComp();
        wordHistogramComp.insert(TEXT);

        System.out.println("The map has " + wordHistogramComp.size() + " distinct words.\n");

        for (String word : wordHistogramComp) {
            System.out.println(word + " : " + wordHistogramComp.get(word));
        }


        WordHistogram wordHistogram = new WordHistogram();
        wordHistogram.insert(TEXT);

        System.out.println("The map has " + wordHistogram.size() + " distinct words.\n");

        for (String word : wordHistogram) {
            System.out.println(word + " : " + wordHistogram.get(word));

        }
    }
}
