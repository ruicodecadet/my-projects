package org.academiadecodigo.fellinux.wordhistogramcomp;

import java.util.HashMap;
import java.util.Iterator;
import java.util.function.Consumer;

public class WordHistogram extends HashMap<String, Integer> implements Iterable<String> {

    //private HashMap<String, Integer> wordHistogram;

    public WordHistogram(){
        //super();
        //this.wordHistogram = new HashMap<>();

    }

    public void insert(String words) {

       String[] splitResult = words.split(" ");

       for (String word : splitResult) {

           if (!containsKey(word)) {
               put(word, 1);
               continue;
           }

           put(word, get(word) + 1);
       }
        //Once again my first solution
       /* for (String word : words.split(" ")) {
            //wordHistogram can be switched for super
            if (super.get(word) == null) {
                super.put(word, 1);
            } else {
                super.put(word, get(word) + 1);
            }
        }*/
    }

    /*public int size() {
        return wordHistogram.size();
    }

    public int get(String word) {

        return wordHistogram.get(word);
    }*/

/*
    @Override
    public void forEach(Consumer<? super String> action) {

    }*/

    @Override
    public Iterator<String> iterator() {
        return super.keySet().iterator();
    }
}
