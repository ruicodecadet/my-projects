package org.academiadecodigo.fellinux.wordhistogramcomp;

import java.util.*;

public class WordHistogramComp implements Iterable<String> {

    private HashMap<String, Integer> wordHistogramComp;

    //private int counter;

    public WordHistogramComp() {
        this.wordHistogramComp = new HashMap<>();

    }

    public void insert(String words) {

        String[] splitResult = words.split(" ");

        for (String word : splitResult) {
            if (!wordHistogramComp.containsKey(word)) {
                wordHistogramComp.put(word, 1);
                continue;
            }
            wordHistogramComp.put(word, get(word) + 1);
        }

        //My first solution
        /*for (String word : words.split(" ")) {
           if (wordHistogramComp.get(word) == null) {
                wordHistogramComp.put(word, 1);
           } else {
               wordHistogramComp.put(word, get(word) + 1);
           }
        }*/
    }

    public int size() {
        return wordHistogramComp.size();
    }

    public int get(String word) {

        return wordHistogramComp.get(word);
    }

    @Override
    public Iterator<String> iterator() {
        return wordHistogramComp.keySet().iterator();
    }
}


