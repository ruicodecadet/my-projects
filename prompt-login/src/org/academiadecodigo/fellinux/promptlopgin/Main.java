package org.academiadecodigo.fellinux.promptlopgin;

public class Main {

    public static void main(String[] args) {

        UserManager userManager = new UserManager();
        userManager.addUser(new User("rui", "1234"));
        userManager.addUser(new User("patricio", "5678"));
        UserInterface userInterface = new UserInterface(userManager);
        userInterface.start();

    }

}
