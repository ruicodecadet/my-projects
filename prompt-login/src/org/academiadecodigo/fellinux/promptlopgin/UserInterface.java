package org.academiadecodigo.fellinux.promptlopgin;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.string.PasswordInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringSetInputScanner;

import java.util.Set;

public class UserInterface {

    private UserManager userManager;
    private Prompt prompt;
    String login = "";
    String pass = "";

    public UserInterface(UserManager userManager) {

        prompt = new Prompt(System.in, System.out);
        this.userManager = userManager;

    }

    public void start() {

        Set<String> options = userManager.keySet();

        System.out.println("Well come to your first Prompt-View Experience\n");

        StringSetInputScanner scanner = new StringSetInputScanner(options);
        scanner.setMessage("Enter your login credentials: ");
        login = prompt.getUserInput(scanner);

        do {

            PasswordInputScanner passwordInputScanner = new PasswordInputScanner();
            passwordInputScanner.setMessage("Enter your password: ");
            pass = prompt.getUserInput(passwordInputScanner);

            System.out.println(checkPassword() ? "Login successful!" : "Try again!");

        } while (!checkPassword());

    }

    public boolean checkPassword() {

        return userManager.getOptions().get(login).equals(pass);

    }

}
