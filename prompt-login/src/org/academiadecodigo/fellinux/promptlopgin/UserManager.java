package org.academiadecodigo.fellinux.promptlopgin;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class UserManager {

    private Map<String, String> options;

    public UserManager() {

        options = new HashMap();

    }

    public void addUser(User user) {

        options.put(user.getLogin(), user.getPassword());

    }

    public Map<String, String> getOptions() {

        return options;

    }

    public Set<String> keySet() {

        return options.keySet();

    }

}
