package org.academiadecodigo.fellinux.filemanager;

public class FileNotFoundException extends FileExceptions {

    public FileNotFoundException() {
        super("The file is not in the container");
    }
}
