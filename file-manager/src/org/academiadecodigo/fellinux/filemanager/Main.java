package org.academiadecodigo.fellinux.filemanager;

import org.academiadecodigo.fellinux.filemanager.*;

public class Main {
    public static void main(String[] args) {

        try {
            File[] container = new File[]{new File(""), new File("a"), new File("3"), new File("j")};
            FileManager files = new FileManager(container);
            files.getFile("b");
            files.getFile("a");
            files.login();
            files.createFile("Rui");
            files.logout();
            files.createFile("Ze");
            files.login();
            files.getFile("Rui");

        } catch (NotEnoughPermissionsExceptions notEnoughPermissionsExceptions) {
            System.out.println(notEnoughPermissionsExceptions.getMessage());
            //System.out.println(notEnoughPermissionsExceptions.getMessage());
        } catch (FileNotFoundException fileNotFoundException) {
            System.out.println(fileNotFoundException.getMessage());
        } catch (NotEnoughSpaceException notEnoughSpaceException) {
            System.out.println(notEnoughSpaceException.getMessage());
        } catch (FileExceptions fileExceptions) {
            System.out.println(fileExceptions.getMessage());
        }


    }
}
