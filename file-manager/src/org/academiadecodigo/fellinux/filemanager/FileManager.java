package org.academiadecodigo.fellinux.filemanager;

import java.io.FileNotFoundException;

public class FileManager {

    private boolean logged;
    private File[] fileContainer;
    //private int fileCounter;
    private String name;

    public FileManager(File[] container) {
        logged = false;
        fileContainer = container;
        name = "";

    }

    public File getFile(String name) throws FileNotFoundException, NotEnoughPermissionsExceptions {

        if(logged) {

            for (File file: fileContainer) {
                if(file.getName().equals(name)) {
                    return file;
                }
            }

            throw new FileNotFoundException();

        }

        throw new NotEnoughPermissionsExceptions();

    }

    public void login(){
        logged = true;
    }

    public void logout() {
        logged = false;
    }

    public void createFile(String name) throws NotEnoughSpaceException, NotEnoughPermissionsExceptions {

        if(logged) {

            for (int i = 0; i < fileContainer.length; i++)  {
                if(fileContainer[i].getName().equals("")) {
                    fileContainer[i] = new File(name);
                    return;
                }
            }

            throw new NotEnoughSpaceException();

        }

        throw new NotEnoughPermissionsExceptions();
    }

}
