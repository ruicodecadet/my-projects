package org.academiadecodigo.fellinux.filemanager;

public class FileExceptions extends Exception {

    public FileExceptions(String message) {
        super(message);
    }
}
