package org.academiadecodigo.fellinux.filemanager;

public class NotEnoughPermissionsExceptions extends FileExceptions{

    public NotEnoughPermissionsExceptions() {
        super("Not logged in. Please do login!");

    }

}
