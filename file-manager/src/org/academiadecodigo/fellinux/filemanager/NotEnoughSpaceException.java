package org.academiadecodigo.fellinux.filemanager;

public class NotEnoughSpaceException extends FileExceptions{

    public NotEnoughSpaceException() {
        super("Not enough space in your container");
    }
}
