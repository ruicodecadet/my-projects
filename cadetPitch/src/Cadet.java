

/*Your program has to create 5 cadets.
        Each cadet has a name and a pitch.
        Your program should ask every cadet to pitch themselves.
        At the end, your program should change the pitch of a random cadet to something even funnier.*/

class Cadet {

    private String name;
    private String pitch;

    public Cadet(String name, String pitch) {
        this.name = name;
        this.pitch = pitch;
    }

    public String getName() {
        return this.name;
    }

    /*public void setName(String name) {
        this.name = name;
    }*/

    public String getPitch() {
        return this.pitch;
    }

    public void setPitch(String pitch) {
        this.pitch = pitch;
    }
}

