import javax.swing.*;

public class Person {

    private String name;
    private Hotel hotel;
    private int roomId = -1;

    public Person(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setHotel(Hotel hotel){
        this.hotel = hotel;
    }

    public boolean askForCheckIn(){

        if(hotel == null || roomId != -1){
            return false;
        }

        roomId = hotel.checkIn();

        if(roomId == -1){
            return false;
        }

        return true;
    }

    public boolean askForCheckOut(){
        if(hotel == null || roomId == -1){
            return false;
        }

        hotel.checkOut(roomId);
        roomId = -1;
        return true;
    }

    /*@Override
    public String toString(){

    }*/
}
