public class Hotel {

    private static final int NO_KEY = -1;
    private String name;
    private Room[] rooms;

    public Hotel(String name, int numRooms){
        this.name = name;
        rooms = new Room[numRooms];
    }

    /*public int hasRoom() {

        int freeRoom;
        for (int i = 0; i < rooms.length; i++) {
            if(rooms[i].getStatus() == false){
                freeRoom = i;
                return freeRoom;
            }
        }
        return -1;
    }*/

    public int checkIn(){

        for(int i =0; i < rooms.length; i++){
            if(rooms[i] == null){
                rooms[i] = new Room(); //This technique is called lazy instantiation
            }

            if(rooms[i].isAvailable()){
                rooms[i].setAvailable(false);
                return i;
            }
        }

        //There are better ways to do this in Java
        //We will learn these latter on!
        return NO_KEY;
    }

    public void checkOut(int roomId){

        if(rooms[roomId] != null){
            rooms[roomId].setAvailable(true);
        }
    }

    public String getName(){
        return name;
    }

    /*@Override
    public String toString(){
        return "
    public */
}



