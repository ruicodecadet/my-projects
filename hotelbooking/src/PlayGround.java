public class PlayGround {
    public static void main(String[] args) {

        Hotel ritz = new Hotel("Ritz", 100);
        Hotel pestana = new Hotel("Pestana", 300);
        Person asterix = new Person("Asterix");
        Person obelix = new Person("Obelix");

        asterix.setHotel(ritz);
        obelix.setHotel(ritz);

        if(!obelix.askForCheckIn()){
            System.out.println("Could not get a room for " + obelix.getName());
        }

        if(!asterix.askForCheckIn()){
            System.out.println("Could not get a room for " + asterix.getName());
        }

        obelix.askForCheckOut();
        asterix.askForCheckOut();

        if(!asterix.askForCheckOut()){
            System.out.println("Unable to checkout twice in a row!");
        }

        if(asterix.askForCheckIn() && !asterix.askForCheckIn()){
            System.out.println("Could no checkin twice!");
        }

        if(asterix.askForCheckOut() && asterix.askForCheckIn()){
            System.out.println("Checkin after checkout works fine!");
        }

        System.out.println(asterix);
        asterix.askForCheckOut();
        asterix.setHotel(pestana);
        asterix.askForCheckIn();

        System.out.println(ritz);
        System.out.println(pestana);
        System.out.println(asterix);
        System.out.println(obelix);
    }
}
