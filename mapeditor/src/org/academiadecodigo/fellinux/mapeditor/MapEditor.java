package org.academiadecodigo.fellinux.mapeditor;

import org.academiadecodigo.fellinux.mapeditor.gridelements.Cell;
import org.academiadecodigo.fellinux.mapeditor.gridelements.Cursor;
import org.academiadecodigo.fellinux.mapeditor.utils.DirectionType;
import org.academiadecodigo.fellinux.mapeditor.utils.FileManager;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

import java.io.File;

public class MapEditor implements KeyboardHandler {

    public static final int COL_NUM = 20;
    public static final int ROW_NUM = 20;
    public static final int CELL_SIZE = 32;
    public static final int PADDING = 10;

    private Grid grid;
    private Cell cell;
    private Keyboard keyboard;
    private Cursor cursor;

    private boolean painting;

    public MapEditor() {

        grid = new Grid(ROW_NUM, COL_NUM);
        cursor = new Cursor();

    }

    public void start() {

    }

    public void moveCursor(DirectionType direction) {

        if (cursorOnEdge(direction)) {
            return;
        }

        switch (direction) {
            case UP:
                cursor.moveUp();
                break;
            case DOWN:
                cursor.moveDown();
                break;
            case RIGHT:
                cursor.moveRight();
                break;
            case LEFT:
                cursor.moveLeft();
                break;
            default:
                break;
        }

        if (painting) {
            paintCell();
        }
    }

    public void paintCell() {
        Cell cell = grid.getCell(cursor.getRow(), cursor.getCol());
        if (cell.isPainted()) {
            cell.erase();
        } else {
            cell.paint();
        }

    }

    public void clear() {
        grid.clear();
    }

    public void load() {
        grid.stringToGrid(FileManager.readFile());
    }

    public void save() {
        FileManager.writeToFile(grid.toString());
    }

    private boolean cursorOnEdge(DirectionType direction) {
        return direction == DirectionType.UP && cursor.getRow() == 0 ||
                direction == DirectionType.DOWN && cursor.getRow() == grid.getRows() - 1 ||
                direction == DirectionType.LEFT && cursor.getCol() == 0 ||
                direction == DirectionType.RIGHT && cursor.getCol() == grid.getCols() - 1;
    }

    public void cursorInit() {

        keyboard = new Keyboard(this);

        KeyboardEvent right = new KeyboardEvent();
        right.setKey(KeyboardEvent.KEY_RIGHT);
        right.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent left = new KeyboardEvent();
        left.setKey(KeyboardEvent.KEY_LEFT);
        left.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent down = new KeyboardEvent();
        down.setKey(KeyboardEvent.KEY_DOWN);
        down.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent up = new KeyboardEvent();
        up.setKey(KeyboardEvent.KEY_UP);
        up.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent space = new KeyboardEvent();
        space.setKey(KeyboardEvent.KEY_SPACE);
        space.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent spaceR = new KeyboardEvent();
        spaceR.setKey(KeyboardEvent.KEY_SPACE);
        spaceR.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);

        KeyboardEvent c = new KeyboardEvent();
        c.setKey(KeyboardEvent.KEY_C);
        c.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent s = new KeyboardEvent();
        s.setKey(KeyboardEvent.KEY_S);
        s.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent l = new KeyboardEvent();
        l.setKey(KeyboardEvent.KEY_L);
        l.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        /**
         * Listeners
         */

        keyboard.addEventListener(left);
        keyboard.addEventListener(right);
        keyboard.addEventListener(up);
        keyboard.addEventListener(down);
        keyboard.addEventListener(space);
        keyboard.addEventListener(spaceR);
        keyboard.addEventListener(c);
        keyboard.addEventListener(s);
        keyboard.addEventListener(l);

    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_UP:
                //cursor.setDirection(DirectionType.UP);
                //cursor.moveUp();
                moveCursor(DirectionType.UP);
                break;
            case KeyboardEvent.KEY_DOWN:
                //cursor.setDirection(DirectionType.DOWN);
                //cursor.moveDown();
                moveCursor(DirectionType.DOWN);
                break;
            case KeyboardEvent.KEY_LEFT:
                //cursor.setDirection(DirectionType.LEFT);
                //cursor.moveLeft();
                moveCursor(DirectionType.LEFT);
                break;
            case KeyboardEvent.KEY_RIGHT:
                //cursor.setDirection(DirectionType.RIGHT);
                //cursor.moveRight();
                moveCursor(DirectionType.RIGHT);
                break;

            case KeyboardEvent.KEY_SPACE:
                setPainting(true);
                paintCell();
                break;

            case KeyboardEvent.KEY_C:
                clear();
                break;

            case KeyboardEvent.KEY_S:
                save();
                break;

            case KeyboardEvent.KEY_L:
                load();
                break;

            default:
                break;

        }

    }

    private void setPainting(boolean painting) {
        this.painting = painting;
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_SPACE) {
            setPainting(false);
        }

    }

}
