package org.academiadecodigo.fellinux.mapeditor.utils;

public enum DirectionType {

    UP(0, -1),
    DOWN(0, 1),
    LEFT(-1, 0),
    RIGHT(1, 0);


    DirectionType(int xMove, int YMove) {
    }

}
