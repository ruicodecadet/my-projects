package org.academiadecodigo.fellinux.mapeditor.gridelements;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class WordReader implements Iterable<String> {

    private String file;

    public WordReader(String filePath) {

        this.file = filePath;

    }

    @Override
    public Iterator<String> iterator() {
        return new WordReaderIterator();
    }

    private class WordReaderIterator implements Iterator<String> {

        private BufferedReader inputBufferedReader;

        private String[] words;
        private String currentLine;
        private int wordsIndex;

        private WordReaderIterator() {

            try {

                wordsIndex = 0;
                inputBufferedReader = new BufferedReader(new FileReader(file));

                //Read the first line of the file
                currentLine = readLineOfText();
                words = getLineWords(currentLine);

            } catch (FileNotFoundException e) {

                throw new IllegalArgumentException(e);
            }
        }

        public String readLineOfText() {

            String line = null;

            try {

                line = inputBufferedReader.readLine();

                //End of file
                if (line == null) {
                    inputBufferedReader.close();
                    return null;
                }

                //Line contains no words or only contains non-word character, fetch
                if (line.equals("") || line.matches("\\W+")) {
                    return readLineOfText();
                }

            } catch (IOException e) {
                currentLine = null;

            }

            return line;
        }

        private String[] getLineWords(String line) {
            return line != null ? line.split("\\W+") : null;
        }


        @Override
        public boolean hasNext() {
            return currentLine != null;
        }

        @Override
        public String next() {

            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            String result = words[wordsIndex];
            wordsIndex++;

            if (wordsIndex == words.length) {
                currentLine = readLineOfText();
                words = getLineWords(currentLine);
                wordsIndex = 0;
            }

            return result;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("remove");
        }


    }

}

