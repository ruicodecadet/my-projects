package org.academiadecodigo.fellinux.mapeditor.gridelements;

import jdk.jfr.Percentage;
import org.academiadecodigo.fellinux.mapeditor.Grid;
import org.academiadecodigo.fellinux.mapeditor.MapEditor;
import org.academiadecodigo.fellinux.mapeditor.utils.DirectionType;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Cursor extends Cell {

    //private boolean isCursor;
    private DirectionType direction;
    private Grid grid;
    private int row;
    private int col;

    public Cursor() {
        super(0, 0);
        rectangle.setColor(Color.GREEN);
        paint();
        row = 0;
        col = 0;
    }

    public void moveUp() {
        //row = getRow();
        row--;
        rectangle.translate(0, -MapEditor.CELL_SIZE);
    }

    public void moveDown() {
        //row = getRow();
        row++;
        rectangle.translate(0, MapEditor.CELL_SIZE);
    }

    public void moveRight() {
        //col = super.getCol();
        col++;
        rectangle.translate(MapEditor.CELL_SIZE, 0);
    }

    public void moveLeft() {
        //col = super.getCol();
        col--;
        rectangle.translate(-MapEditor.CELL_SIZE, 0);
    }

    @Override
    public int getRow() {
        return row;
    }

    @Override
    public int getCol() {
        return col;
    }

    /* public void beCursor(int col, int row, int cellSize) {
        isCursor = true;
        super.cellDraw(col, row, cellSize, Color.GREEN);
    }

    public void setDirection(DirectionType direction) {
        this.direction = direction;
    }

    @Override
    public void cellDraw(int col, int row, int cellSize, Color color) {
        *//*super.setColor(Color.GREEN);
        super.fill();*//*

    }

    public void move(DirectionType direction) {

        switch (direction) {
            case UP:
                numUp = this.getRow();
                System.out.println(numUp);
                if (super.getRow() >= 0) {
                    super.cellMove(0, -(Grid.CELL_SIZE));
                }
                //row = 0;
                break;

            case DOWN:
                numDown = this.getRow();
                System.out.println(numDown);
                if (this.getRow() < Grid.ROW_NUM) {
                    this.cellMove(0, Grid.CELL_SIZE);
                }
                //row = Grid.ROW_NUM - 1;
                break;

            case LEFT:
                numLeft = this.getCol();
                System.out.println(numLeft);
                if (this.getCol() >= 0) {
                    this.cellMove(-(Grid.CELL_SIZE), 0);
                }
                //col = 0;
                break;

            case RIGHT:
                numRight = this.getCol();
                System.out.println(numRight);
                if (this.getCol() < Grid.COL_NUM) {
                    this.cellMove(Grid.CELL_SIZE, 0);
                }
                //col = Grid.COL_NUM - 1;
                break;

        }

    }*/

}
