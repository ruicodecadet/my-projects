package org.academiadecodigo.fellinux.mapeditor.gridelements;

import org.academiadecodigo.fellinux.mapeditor.Grid;
import org.academiadecodigo.fellinux.mapeditor.MapEditor;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Cell {

    private int col;
    private int row;
    Rectangle rectangle;
    private boolean painted;

    public Cell(int row, int col) {
        this.row = row;
        this.col = col;
        rectangle = new Rectangle(MapEditor.PADDING + col * MapEditor.CELL_SIZE, MapEditor.PADDING + row * MapEditor.CELL_SIZE, MapEditor.CELL_SIZE, MapEditor.CELL_SIZE);
        rectangle.draw();

    }

    public void paint() {
        painted = true;
        rectangle.fill();
    }

    public void erase() {
        painted = false;
        rectangle.draw();
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public boolean isPainted() {
        return painted;
    }

    @Override
    public String toString(){
        return painted ? "1" : "0";
    }

}
