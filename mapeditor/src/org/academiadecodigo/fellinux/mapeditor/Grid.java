package org.academiadecodigo.fellinux.mapeditor;

import org.academiadecodigo.fellinux.mapeditor.gridelements.Cell;
import org.academiadecodigo.fellinux.mapeditor.gridelements.WordReader;
import org.academiadecodigo.fellinux.mapeditor.utils.FileManager;

public class Grid {

    private int cols;
    private int rows;
    private Cell[][] cells;

    public Grid(int rows, int cols) {

        this.rows = rows;
        this.cols = cols;

        cells = new Cell[rows][cols];

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                cells[row][col] = new Cell(row, col);
            }

        }

    }

    public int getCols() {
        return cols;
    }

    public int getRows() {
        return rows;
    }


    public Cell getCell(int row, int col) {
        return cells[row][col];
    }

    public void clear() {
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                cells[row][col].erase();
            }

        }

    }

    public void stringToGrid(String grid) {

        int index = 0;

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {

                Cell cell = cells[row][col];

                if (grid.charAt(index) == '0') {
                    cell.erase();
                } else {
                    cell.paint();
                }

                index++;

            }

            index++;

        }

    }

  /*  @Override
    public String toString() {

        WordReader wordReader = new WordReader(FileManager.readFile());
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++){
                cells[row][col] = ;
            }
        }
        return ;

    }*/

}
