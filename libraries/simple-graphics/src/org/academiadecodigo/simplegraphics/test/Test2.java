package org.academiadecodigo.simplegraphics.test;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Test2 {
    public static void main(String[] args) {

        int cols = 10;
        int rows = 10;
        int size = 50;
        int padding = 10;
        int space = 20;

        //r1.fill();

        Rectangle[][] rectArray = new Rectangle[cols][rows];


        for (int i = 0; i < rectArray[cols - 1].length; i++) {
            for (int j = 0; j < rectArray[rows - 1].length; j++) {

                rectArray[i][j] = new Rectangle(padding + (space + size) * i, padding + (space + size) * j, size, size);
                rectArray[i][j].setColor(new Color(123, 40, 30));
                rectArray[i][j].fill();
            }
        }
    }
}
