package org.academiadecodigo.simplegraphics.test;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Test1 {
    public static void main(String[] args) {

        int cols = 10;
        int size = 50;
        int padding = 10;
        int space = 20;

        //r1.fill();

        Rectangle[] rectArray = new Rectangle[cols];


        for (int i = 0; i < rectArray.length; i++) {

            rectArray[i] = new Rectangle(padding + (space + size) * i, padding, size, size);;
            rectArray[i].setColor(new Color(123, 40, 30));
            rectArray[i].fill();
        }

        /*for (int i = 0; i < rectArray.length; i++) {
            rectArray[i].fill();
            rectArray[i].translate(60, 0);
        }*/

    }



}
