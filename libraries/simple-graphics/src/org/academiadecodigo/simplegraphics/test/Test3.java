package org.academiadecodigo.simplegraphics.test;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Test3 {
    public static void main(String[] args) throws InterruptedException {

        int cols = 20;
        int rows = 20;
        int size = 25;
        int padding = 5;
        int space = 5;

        //r1.fill();

        Rectangle[][] rectArray = new Rectangle[cols][rows];


        for (int i = 0; i < rectArray[cols - 1].length; i++) {

            for (int j = 0; j < rectArray[rows - 1].length ; j++) {

                rectArray[i][j] = new Rectangle(padding + (space + size) * i, padding + (space + size) * i, size, size);
                rectArray[i][j].setColor(new Color(123, 40, 30));
                rectArray[i][j].fill();

                rectArray[i][j] = new Rectangle(padding + (space + size) * i, padding + (space + size) * j, size, size);
                rectArray[i][j].setColor(new Color(123, 40, 30));
                rectArray[i][j].fill();
            }
          /*  for (int j = 0; j < rectArray[rows - 1].length; j++) {

                rectArray[i][j] = new Rectangle(padding + (space + size) * j, padding + (space + size) * j, size, size);
                rectArray[i][j].setColor(new Color(123, 40, 30));
                rectArray[i][j].fill();
            }*/
        }

        /*for (int i = 10; i < rectArray[cols - 1].length; i++) {
            for (int j = 0; j < rectArray[rows - 1].length; j++) {

                rectArray[i][j] = new Rectangle(padding + (space + size) * j, padding + (space + size) * j, size, size);
                rectArray[i][j].setColor(new Color(123, 40, 30));
                rectArray[i][j].fill();
            }
            for (int j = 0; j < rectArray[rows - 1].length; j++) {

                rectArray[i][j] = new Rectangle(padding + (space + size) * i, padding + (space + size) * i, size, size);
                rectArray[i][j].setColor(new Color(123, 40, 30));
                rectArray[i][j].fill();
            }
        }*/
        /*for (int i = 10; i < rectArray[cols - 1].length; i++) {
            for (int j = 0; j < rectArray[rows - 1].length -10; j++) {

                rectArray[i][j] = new Rectangle(padding + (space + size) * j, padding + (space + size) * j, size, size);
                rectArray[i][j].setColor(new Color(123, 40, 30));
                rectArray[i][j].fill();
            }
        }*/
    }
}