package org.academiadecodigo.bootcamp.containers;

public class LinkedList<T> {

    private Node head;
    private int length = 0;

    public LinkedList() {
        this.head = new Node(null);
    }

    public int size() {
        return length;
    }

    /**
     * Adds an element to the end of the list
     * @param data the element to add
     */
    public void add(T data)  {

        Node node = new Node(data);
        Node iterator = head;
        while (iterator.getNext() != null){
            iterator = iterator.getNext();
        }
        iterator.setNext(node);
        length++;

    }

    /**
     * Obtains an element by index
     * @param index the index of the element
     * @return the element
     */
    public T get(int index) {
        Node iterator = head.getNext();

        if(index > length) {
            return null;
        }

        while(iterator != null) {
            if(index == 0) {
                return iterator.getData();
            }
            iterator = iterator.getNext();
            index--;
        }

        //For i seems to be ugly!!!
        /*for(int i = 0; i < size(); i++){
            iterator = iterator.getNext();
            if(i == index){
                return iterator.getData();
            }
        }*/
        return null;
        //throw new UnsupportedOperationException();
    }

    /**
     * Returns the index of the element in the list
     * @param data element to search for
     * @return the index of the element, or -1 if the list does not contain element
     */
    public int indexOf(T data) {
        int index = 0;

        Node iterator = head;

        while (iterator.getNext() != null) {
            iterator = iterator.getNext();
            if(iterator.getData().equals(data)) {
                return index;
                }
            index++;
            }
        //System.out.println(index);
        return -1;

        //throw new UnsupportedOperationException();
    }

    /**
     * Removes an element from the list
     * @param data the element to remove
     * @return true if element was removed
     */
    public boolean remove(T data) {

        Node iterator = head;
        iterator = iterator.getNext();
        Node iterator2 = head;

        while (iterator != null) {

            if(iterator.getData().equals(data)) {
                iterator2.setNext(iterator.getNext());
                length--;
                return true;
            }
            iterator2 = iterator;
            iterator = iterator.getNext();

        }

        //throw new UnsupportedOperationException();
        return false;
    }

    /**
     * Removes an element from the list
     * @param index the element to remove
     * @return true if element was removed
     */
    public boolean remove1(int index) {

        Node iterator = head.getNext();
        //iterator = iterator.getNext();
        Node previous = head;

        while (iterator != null) {

            if(index == 0) {
                previous.setNext(iterator.getNext());
                length--;
                return true;
            }

            previous = iterator;
            iterator = iterator.getNext();
            index--;

        }

        //throw new UnsupportedOperationException();
        return false;
    }

    private class Node {

        private T data;
        private Node next;

        public Node(T data) {
            this.data = data;
            next = null;
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }

}
