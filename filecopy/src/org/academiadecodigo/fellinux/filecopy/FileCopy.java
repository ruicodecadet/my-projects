package org.academiadecodigo.fellinux.filecopy;

import javax.swing.*;
import java.io.*;

public class FileCopy {

    private static final String PATH = "resources/";

    public void copy(String src, String dst) {

        FileInputStream input = null;
        FileOutputStream output = null;

        try {

            input = new FileInputStream(PATH + src);
            output = new FileOutputStream(PATH + dst);

            byte[] buffer = new byte[2048];
            int bytesRead = input.read(buffer);

            while (bytesRead != -1) {

                System.out.println("Bytes read " + bytesRead);

                output.write(buffer, 0, bytesRead);
                bytesRead = input.read(buffer);

            }

        }

        catch (FileNotFoundException exception) {

            exception.printStackTrace();

        }

        catch (IOException ex) {

            ex.printStackTrace();

        }

        finally {

            closeStream(input);
            closeStream(output);

        }

    }

    private void closeStream(Closeable toClose) {

        if (toClose == null) {

            return;

        }

        try {

            toClose.close();

        }

        catch (IOException e) {

            e.printStackTrace();

        }

    }

}
