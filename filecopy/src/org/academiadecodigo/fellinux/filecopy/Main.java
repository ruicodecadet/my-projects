package org.academiadecodigo.fellinux.filecopy;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        FileCopy fileCopy = new FileCopy();

        String src = "filetocopy";
        String dst = "copyoffile";

        fileCopy.copy(src, dst);

    }
}
