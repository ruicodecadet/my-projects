package org.academiadecodigo.felinux.sniper;

public class Main {
    public static void main(String[] args) {

        // create a new game with the specified number of objects
        Game game = new Game(20);

        // start the game
        game.start();
    }
}
