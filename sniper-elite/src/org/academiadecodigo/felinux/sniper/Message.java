package org.academiadecodigo.felinux.sniper;

public class Message {
    public static final String TREE = "Sniper: 🌲 Tree!";
    public static final String ENEMY = "Sniper: 🦾 An enemy! Attack‼️";
    public static final String FAILED_HIT = "Sniper: 💢 Failed !!";
    public static final String ARMOUR_HIT = "Enemy: 🛡️ Hit! Armour remaining: ";
    public static final String ARMOUR_DESTROYED = "Enemy: 🛡️ Armour destroyed!";
    public static final String REMAINING_HEALTH = "Enemy: 💚 Hit! Health remaining: ";
    public static final String ENEMY_DEATH = "Sniper: Fatal shot! On to the next one!";
    public static final String ENEMY_DIED = "Enemy: 💔 Died!!";
    public static final String BARREL_DESTROYED = "Barrel exploded";
}
