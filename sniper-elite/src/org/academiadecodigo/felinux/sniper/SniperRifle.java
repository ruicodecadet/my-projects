package org.academiadecodigo.felinux.sniper;

import org.academiadecodigo.felinux.sniper.gameobject.Destroyable;
import org.academiadecodigo.felinux.sniper.gameobject.Enemy;

/**
 * A sniper rifle used to kill enemies
 */
public class SniperRifle {

    private static final int SHOT_DAMAGE = 10;
    private static final double HITTING_PROBABILITY = 0.3;

    /**
     * Take a shoot at an enemy
     * @param enemy the enemy to shoot
     */
    public void shoot(Destroyable enemy) {

        if (Math.random() < HITTING_PROBABILITY){
            enemy.hit(SHOT_DAMAGE);
            return;
        }

        System.out.println(Message.FAILED_HIT);

    }
}
