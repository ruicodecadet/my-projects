package org.academiadecodigo.felinux.sniper.gameobject;

import org.academiadecodigo.felinux.sniper.Message;

/**
 * A simple tree, not worthy of getting shot
 */
public class Tree extends GameObject {

    /**
     * @see GameObject#getMessage()
     */
    @Override
    public String getMessage() {
        return Message.TREE;
    }
}
