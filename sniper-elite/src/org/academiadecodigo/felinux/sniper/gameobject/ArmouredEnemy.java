package org.academiadecodigo.felinux.sniper.gameobject;

import org.academiadecodigo.felinux.sniper.Message;

/**
 * An enemy with an armour
 */
public class ArmouredEnemy extends Enemy {

    private static final int MAX_ARMOUR = 25;
    private int armour;

    /**
     * Constructs an armoured enemy
     */
    public ArmouredEnemy() {
        this.armour = MAX_ARMOUR;
    }

    /**
     * Damage hits armour, if armour is lost it proceeds to take damage
     * @see Enemy#hit(int)
     */
    @Override
    public void hit(int damage) {

        if (damage < armour) {
            this.armour -= damage;
            System.out.println(Message.ARMOUR_HIT + armour);
            return;
        }

        System.out.println(Message.ARMOUR_DESTROYED);
        super.hit(damage - armour);
        this.armour = 0;
    }
}
