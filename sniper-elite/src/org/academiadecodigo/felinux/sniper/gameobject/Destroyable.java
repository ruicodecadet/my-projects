package org.academiadecodigo.felinux.sniper.gameobject;

public interface Destroyable {

    void hit(int damage);

    boolean isDestroyed();

}
