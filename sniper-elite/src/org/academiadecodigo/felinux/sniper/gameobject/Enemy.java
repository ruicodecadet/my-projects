package org.academiadecodigo.felinux.sniper.gameobject;

import org.academiadecodigo.felinux.sniper.Message;

/**
 * An enemy class containing generic enemy functionality and meant for subclassing
 */
public abstract class Enemy extends GameObject implements Destroyable{

    private static final int MAX_HEALTH = 100;
    private int health;
    private boolean isDead;

    /**
     * Generic enemy constructor
     */
    public Enemy() {
        this.health = MAX_HEALTH;
        this.isDead = false;
    }

    /**
     * Removes health according to the hit damage
     * @param damage the damage impact
     */
    public void hit(int damage) {

        this.health = (damage > this.health) ? 0 : this.health - damage;

        if (health <= 0) {
            System.out.println(Message.ENEMY_DEATH);
            isDead = true;
            return;
        }

        System.out.println(Message.REMAINING_HEALTH + health);
    }

    /**
     * @see GameObject#getMessage()
     */
    @Override
    public String getMessage() {
        return Message.ENEMY;
    }

    public boolean isDead() {
        return isDead;
    }

    public boolean isDestroyed() {
        return isDead;
    }

}
