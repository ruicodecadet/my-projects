package org.academiadecodigo.felinux.sniper.gameobject;

import org.academiadecodigo.felinux.sniper.Message;

public class Barrel extends GameObject implements Destroyable{
    private BarrelType barrelType;
    private int currentDamage;
    private boolean destroyed;

    public Barrel(){

        int index = (int) (Math.random() * (BarrelType.values().length));
        barrelType = BarrelType.values() [index];

    }

    @Override
    public void hit(int damage) {
        currentDamage++;
        if(currentDamage >= barrelType.getMaxDamage()) {
            destroyed = true;
        }
    }

    @Override
    public boolean isDestroyed() {
      return destroyed;
    }

    @Override
    public String getMessage() {
        return Message.BARREL_DESTROYED;
    }
}
