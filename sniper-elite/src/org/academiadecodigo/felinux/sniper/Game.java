package org.academiadecodigo.felinux.sniper;

import org.academiadecodigo.felinux.sniper.gameobject.*;

/**
 * Sniper Elite
 */
public class Game {

    private static final double TREE_PROBABILITY = 0.2;
    private static final double ARMOURED_PROBABILITY = 0.25;
    private static final double BARREL_PROBABILITY = 0.3;

    private final SniperRifle sniperRifle;
    private int shotsFired;
    private GameObject[] gameObjects;

    /**
     * Construct the game
     * @param nObjects the number of game objects to create
     */
    public Game(int nObjects) {
        this.gameObjects = createObjects(nObjects);
        this.sniperRifle = new SniperRifle();
        this.shotsFired = 0;
    }

    /**
     * Start shooting
     */
    public void start() {

        for (GameObject object : gameObjects) {

            System.out.println(object.getMessage());

            if (!(object instanceof Destroyable)) {
                continue;
            }

            Destroyable enemy = (Destroyable) object;

            while (!enemy.isDestroyed()) {
                sniperRifle.shoot(enemy);
                shotsFired++;
            }
            System.out.println(Message.ENEMY_DIED);

        }

        System.out.println("Game ended, " + shotsFired + " were fired");
    }

    /**
     * Creates an array of Gameobjects
     * @param nObjects the number of game objects to create
     */
    private GameObject[] createObjects(int nObjects) {

        GameObject[] gameObjects = new GameObject[nObjects];

        for (int i = 0; i < gameObjects.length; i++) {
            gameObjects[i] = Math.random() < TREE_PROBABILITY ? new Tree() : createEnemy();

        }

        return gameObjects;
    }

    /**
     * Creates one of existing enemies
     */
    private GameObject createEnemy(){

        return Math.random() < ARMOURED_PROBABILITY ? new ArmouredEnemy() : createBarrel();
    }

    private GameObject createBarrel(){

        return Math.random() < BARREL_PROBABILITY ? new Barrel() : new SoldierEnemy();
    }
}
