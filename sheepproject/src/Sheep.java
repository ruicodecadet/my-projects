/*
Your program should create 100 sheep.
        99 sheep have the colour white
        1 sheep has the colour black
        Your program should count every sheep, one by one and print the current number.
        Every time a sheep is counted, it meeeeehs.
        Black and White sheep don't meeeeh the same.
        At the end your  program should change the colour of every sheep to pink.
*/

public class Sheep {
    private String color;
    private String meeeeh;

    public Sheep(String color, String meeeeh){
        this.color = color;
        this.meeeeh = meeeeh;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMeeeeh() {
        return meeeeh;
    }

    public void setMeeeeh(String meeeeh) {
        this.meeeeh = meeeeh;
    }
}
