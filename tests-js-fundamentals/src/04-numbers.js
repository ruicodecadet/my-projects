/**
 * Convert a binary String to a Number
 */
exports.binaryToDecimal = function(str) {
    const intNum = Number.parseInt(str, 2);
    return intNum;
};

/**
 * Add two Numbers with a precision of 2
 */
exports.add =  function(a, b) {
    var num = parseFloat(a+b).toFixed(2);
    return +num;
};

/**
 * Multiply two Numbers with a precision of 4
 */
exports.multiply =  function(a, b) {
    var num1 = parseFloat(a * b).toPrecision(6);
    return +num1;
};
