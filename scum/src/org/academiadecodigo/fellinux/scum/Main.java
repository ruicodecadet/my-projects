package org.academiadecodigo.fellinux.scum;

import org.academiadecodigo.fellinux.scum.function.BiOperation;
import org.academiadecodigo.fellinux.scum.function.MonoOperation;
import org.academiadecodigo.fellinux.scum.machine.Machine;
import org.academiadecodigo.fellinux.scum.machine.Operation;

public class Main {

    public static void main(String[] args) {

        /* BiOperation<Double, Boolean> comparePrice = (price, cost) -> price > cost;
        MonoOperation<Double> getRRP = price -> price * 1.23;*/

        Machine<Double, Boolean> machine = new Machine<>();
        //System.out.println("The decision to sell is: " + (machine.perform(100.15, 88.00, comparePrice) ? "sell!" : "don't sell!"));
        System.out.println("The decision to sell is: " + (machine.perform(100.15, 88.00, Operation.COMPAREPRICE.getComparePrice()) ? "sell!" : "don't sell!"));

        Machine<Double, Boolean> machine1 = new Machine<>();
        //System.out.println("The RRP price is: " + machine1.perform(100.00, getRRP));
        System.out.println("The RRP price is: " + machine1.perform(100.00, Operation.DORRP.getDoRRP()));

        BiOperation<Integer, Integer> fact = (num, acc) -> factorial(num, acc);
        Machine<Integer, Integer> machine2 = new Machine<>();
        System.out.println("Factorial is: " + machine2.perform(5, 1, fact));

        }

        public static Integer factorial(int num, int acc) {

            if (num == 0 ) {
                return acc;
            }

            return factorial(num - 1, num * acc);

        }

}
