package org.academiadecodigo.fellinux.scum.function;

public interface BiOperation<T, E> {
    E compare(T price, T cost);
}
