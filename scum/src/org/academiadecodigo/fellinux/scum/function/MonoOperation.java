package org.academiadecodigo.fellinux.scum.function;

public interface MonoOperation<T> {
    T addVat(T price);
}
