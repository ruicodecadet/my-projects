package org.academiadecodigo.fellinux.scum.machine;

import org.academiadecodigo.fellinux.scum.function.BiOperation;
import org.academiadecodigo.fellinux.scum.function.MonoOperation;

public class Machine<T, E> {

    public T perform(T price, MonoOperation<T> RRP) {
        return RRP.addVat(price);
    }

    public E perform(T price, T cost, BiOperation<T, E> decision) {
        return decision.compare(price, cost);
    }

}
