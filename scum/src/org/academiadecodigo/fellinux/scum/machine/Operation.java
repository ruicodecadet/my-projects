package org.academiadecodigo.fellinux.scum.machine;

import org.academiadecodigo.fellinux.scum.function.BiOperation;
import org.academiadecodigo.fellinux.scum.function.MonoOperation;

public enum Operation {

    COMPAREPRICE((price, cost) -> price > cost),
    DORRP(price -> (price * 1.23));

    BiOperation<Double, Boolean> comparePrice;
    MonoOperation<Double> doRRP;

    Operation(BiOperation<Double, Boolean> comparePrice) {
        this.comparePrice = comparePrice;
    }

    Operation(MonoOperation<Double> doRRP) {
        this.doRRP = doRRP;
    }

    public BiOperation<Double, Boolean> getComparePrice() {
        return comparePrice;
    }

    public MonoOperation getDoRRP() {
        return doRRP;
    }
}
