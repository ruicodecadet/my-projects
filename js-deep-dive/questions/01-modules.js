/**
 * Creates a counter module with an initial value, zero if not provided
 */
exports.createCounter = function(counter) {
    
    var count = counter;

    if(counter == undefined) {
       counter = 0;
    }


    return (
        get = function(){
            
        }
    );

};

/**
 * Creates a person module with name and age
 * An initial name value should be provided and
 * an exception thrown if not
 */
exports.createPerson = function(name) {

    return {
        age: 0,
        name: name,
    };
};
