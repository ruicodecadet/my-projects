public class Cat extends Fellinux {

    private String name;

    public Cat (String name) {
        super(name);
    }

    public String getName() {
        return name;
    }
}
