public class Tiger extends Fellinux {

    private String name;

    public Tiger (String name) {
        super(name);
    }

    @Override
    public void roar() {
        System.out.println("Roarrrr, roarrrr, roarrrr!");
    }
}
