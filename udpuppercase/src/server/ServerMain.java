package server;

import java.io.IOException;
import java.net.*;

public class ServerMain {

    public static void main(String[] args) throws IOException {

        String convertedString;

        // STEP1: Get your host and port
        String hostName = args[0];
        int portNumber = Integer.parseInt(args[1]);

        // STEP2: Create send and receive buffers
        byte[] sendBuffer = new byte[1024];
        byte[] recvBuffer = new byte[1024];

        // STEP3: Open a UDP (datagram) socket
        DatagramSocket socket = new DatagramSocket(portNumber);

        // STEP4.1: Create and receive UDP datagram packet from the socket
        DatagramPacket receivePacket = new DatagramPacket(recvBuffer, recvBuffer.length);
        socket.receive(receivePacket); // blocks while packet not received

        convertedString = convert(receivePacket.getData());

        System.out.println();

       /* for (int i = 0; i < sendBuffer.length; ++) {
            sendBuffer[i] = convertedString.charAt(i);
        }*/

        // STEP4.2: Create and send UDP datagram packet from the socket
        DatagramPacket sendPacket = new DatagramPacket(sendBuffer,
                sendBuffer.length, InetAddress.getByName(hostName), portNumber);
        socket.send(sendPacket);

        // STEP5: Close the socket
        socket.close();

    }

    public static String convert(byte[] words) {
        String string;
        string = words.toString().toUpperCase();
        //System.out.println(string);
        return string;
    }

}
