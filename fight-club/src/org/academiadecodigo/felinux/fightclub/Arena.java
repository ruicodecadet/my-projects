package org.academiadecodigo.felinux.fightclub;

public class Arena {

    //COMPOSITION
    private Fighter[] fighters;


    public Arena(int nFighters) {
        //encapsulating big logic
        this.fighters = createFighters(nFighters);

    }

    private Fighter[] createFighters(int nFighters) {
        Fighter[] fighters = new Fighter[nFighters];
        int randomIndex = 0;

        for (int i = 0; i < nFighters; i++) {

            // get random integer, for what? to get a reandom fighter type
            randomIndex = (int) Math.floor(Math.random() * FighterType.values().length);
            FighterType fighterType = FighterType.values()[randomIndex];

            switch (fighterType) {
                case SUMO:
                    fighters[i] = new Sumo();
                    break;
                case TAIKONDO:
                    fighters[i] = new Taikondo();
                    break;
                case ESGRIMI:
                    fighters[i] = new Esgrimi();
                    break;
                case BOXER:
                    fighters[i] = new Boxer();
                    break;
            }
            /*
            prob = Math.random();
            if (prob < 0.25) {
                fighters[i] = new Sumo();
                break;
            }
            if (prob < 0.50) {
                fighters[i] = new Taikondo();
                break;
            }
            if (prob < 0.75) {
                fighters[i] = new Esgrimi();
                break;
            }
            fighters[i] = new Boxer();*/
        }
            return fighters;
    }
    public void ringTheBell() {
        //array fightersr
        //fight 101
        for (int i = 0; i < fighters.length-1; i+=2) {
            fighters[i].fight(fighters[i+1]);
        }
    }
}
