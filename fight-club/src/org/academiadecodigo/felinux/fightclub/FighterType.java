package org.academiadecodigo.felinux.fightclub;

public enum FighterType {
    SUMO(100, 30, 20, 100),
    TAIKONDO(49, 22, 100, 12),
    ESGRIMI(47,29,99,1),
    BOXER(26,12,12,12);

    private int health;
    private int stamina;
    private int defense;
    private int strength;

    FighterType(int health, int stamina, int defense, int strength){
        this.health = health;
        this.stamina = stamina;
        this.defense = defense;
        this.strength = strength;

    }

    public int getHealth() {
        return health;
    }

    public int getStamina() {
        return stamina;
    }

    public int getDefense() {
        return defense;
    }

    public int getStrength() {
        return strength;
    }
}
