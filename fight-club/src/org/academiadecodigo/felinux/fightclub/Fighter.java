package org.academiadecodigo.felinux.fightclub;

//ABSTRACT can't be instanciated
public abstract class Fighter {

    // DECLARING properties;
    private int health;
    private int stamina;
    private int defense;
    private int strenght;

    public Fighter(FighterType type){
        // INICIALIZATING
        this.health = type.getHealth();
        this.stamina = type.getStamina();
        this.defense = type.getDefense();
        this.strenght = type.getStrength();

        System.out.println(type + " was created.");

    }

    // INSTANCE METHODE - why? it's non-static.
    // A CLASS that cannot be instatiated can have instance methods???
    // why? They will be inherited by the instanciable subclasses
    public void fight(Fighter opponent){
        this.attack(opponent);
        opponent.attack(this);

    }

    public void attack(Fighter opponent){
        opponent.health -= this.strenght/opponent.defense;
        this.stamina -= this.strenght/opponent.defense * 10;
        System.out.println("Attack: - " + this.strenght/opponent.defense + " health");
        System.out.println("Attack: - " + (this.strenght/opponent.defense * 10) + " health");
    }

    public int getHealth() {
        return health;
    }

    public int getStamina() {
        return stamina;
    }

    public int getDefense() {
        return defense;
    }

    public int getStrenght() {
        return strenght;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void setStamina(int stamina) {
        this.stamina = stamina;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public void setStrenght(int strenght) {
        this.strenght = strenght;
    }
}
