package org.academiadecodigo.felinux.fightclub;

public class Boxer extends Fighter {
    public Boxer() {
        super(FighterType.BOXER);
    }
}
