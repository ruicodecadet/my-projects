package org.academiadecodigo.fellinux.ipfetch;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class Main {

    public static final String API_PREFIX = "http://ipinfo.io/";
    public static final String API_SUFFIX = "/json";

    public static void main(String[] args) {

        String api = args.length > 0 ? API_PREFIX + args[0] + API_SUFFIX : API_PREFIX + API_SUFFIX.substring(1);

        try {

            ObjectMapper mapper = new ObjectMapper();
            IpFetcher location = mapper.readValue(new URL(api), IpFetcher.class);

            System.out.println(location);

        } catch (MalformedURLException ex) {

            System.out.println("Invalid API URL: " + api);

        } catch (IOException ex) {

            ex.printStackTrace();

        }

    }

}

