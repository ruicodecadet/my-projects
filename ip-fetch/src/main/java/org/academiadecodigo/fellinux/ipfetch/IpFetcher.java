package org.academiadecodigo.fellinux.ipfetch;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//ipconfig added another property which is phone, this annotation ignores properties that are not in the pojo when serializing.
@JsonIgnoreProperties(ignoreUnknown = true)
public class IpFetcher {

    private String ipAddress;
    private String hostname;
    private String city;
    private String region;
    private String country;
    private String local;
    private String org;
    private String postal = "Undefined";

    public String getIpAddress() {
        return ipAddress;
    }

    public String getHostname() {
        return hostname;
    }

    public String getCity() {
        return city;
    }

    public String getRegion() {
        return region;
    }

    public String getCountry() {
        return country;
    }

    public String getLocal() {
        return local;
    }

    public String getOrg() {
        return org;
    }

    public String getPostal() {
        return postal;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    @Override
    public String toString() {

        return "IpLocation{" +
                "ip='" + ipAddress + '\'' +
                ", hostname='" + hostname + '\'' +
                ", city='" + city + '\'' +
                ", region='" + region + '\'' +
                ", country='" + country + '\'' +
                ", local='" + local + '\'' +
                ", org='" + org + '\'' +
                ", postal='" + postal + '\'' +
                '}';
    }

}
