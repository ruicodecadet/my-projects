package org.academiadecodigo.felinux;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaTest {

    public static void main(String[] args) {

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("testunit");

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        System.out.println("Result: " +
                entityManager.createNativeQuery("SELECT 1 + 1").getSingleResult());

        entityManager.close();

        entityManagerFactory.close();

    }

}
