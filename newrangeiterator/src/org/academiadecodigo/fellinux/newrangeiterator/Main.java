package org.academiadecodigo.fellinux.newrangeiterator;

public class Main {

    public static void main(String[] args) {

        Range range = new Range(10, 20);


       for (Integer number : range) {
            System.out.println("The number is " + number);
        }

        System.out.println("\n");
        range.setUp(false);

        for (Integer number : range) {
            System.out.println("The number is " + number);
        }
    }
}
