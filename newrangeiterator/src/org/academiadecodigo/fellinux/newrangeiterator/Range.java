package org.academiadecodigo.fellinux.newrangeiterator;

import java.util.Iterator;

public class Range implements Iterable<Integer> {

    private int lowRange;
    private int highRange;
    private boolean up;


    public Range(int minRange, int maxRange) {
        this.lowRange = minRange;
        this.highRange = maxRange;
        this.up = true;
    }

    public void setUp(boolean up) {
        this.up = up;
    }

    public Iterator<Integer> iterator() {

        if(up) {

            return new Iterator<Integer>() {
                private int currentNumber = lowRange;
                @Override
                public boolean hasNext() {
                    return currentNumber <= highRange;
                }

                @Override
                public Integer next() {
                    return currentNumber++;
                }
            };
        }

        return new Iterator<Integer>() {
            private int currentNumber = highRange;
            @Override
            public boolean hasNext() {
                return currentNumber >= lowRange;
            }

            @Override
            public Integer next() {
                return currentNumber--;
            }
        };

        //Different solution that seems to be nice but the 2 anonymous classes are better
        /*return new Iterator<Integer>() {
            private int currentNumber = (up ? highRange : lowRange);
            @Override
            public boolean hasNext() {
                return (up ? currentNumber >= lowRange : currentNumber <= highRange);
            }

            @Override
            public Integer next() {
                return up ? currentNumber-- : currentNumber++;
            }
        };*/

    }

}
