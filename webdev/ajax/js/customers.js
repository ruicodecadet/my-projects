var API_URL = 'http://localhost:8080/javabank5/api/customer';

window.onload = function() {
    fetchCustomers();
};

/* function successCallback(response) {
    // do something with the data
}
 */
function errorCallback(request, status, error) {
    // do something with the error
}

// perform an ajax http get request
$.ajax({
    url: 'http://localhost:8080/javabank5/api/customer',
    async: true,
    success: populateCustomers,
    error: errorCallback
});

function populateCustomers(customerData) {

    var elementStr;
    //var customersTable = document.getElementById("customer-table");
    //var row;

    customerData.forEach(function(element) {
        elementStr =
            "<tr><td>" +
            element.firstName +
            "</td>" +

            "<td>" +
            element.lastName +
            "</td>" +

            "<td>" +
            element.email +
            "</td>" +

            "<td>" +
            element.phone +
            "</td>" +

            '<td><button type="button" id="edit-btn-' +
            element.id +
            '" class="edit-btn btn btn-success">edit</button></td>' +
            '<td><button type="button" id="remove-btn-' +
            element.id +
            '" class="remove-btn btn btn-danger">delete</button></td></tr>';

        $(elementStr).appendTo('.table');
        
        /* row = customersTable.insertRow(-1);
        row.innerHTML = elementStr;

        row.setAttribute("id", "custumer-" + element.id);
        row.setAttribute("class", "customer-data"); */
    });
}