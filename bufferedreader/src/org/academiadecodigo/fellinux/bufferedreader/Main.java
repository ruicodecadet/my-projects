package org.academiadecodigo.fellinux.bufferedreader;

/**
 * Entry point class to the WordReader exercise
 */
public class Main {

    private static final String FILE_PATH = "resources/loren";

    /**
     * *Entry point
     * @param args application entry point arguments
     */

    public static void main(String[] args) {

      WordReader wordReader = new WordReader(FILE_PATH);

      for (String word : wordReader) {
          System.out.println(word);
      }

    }
}
