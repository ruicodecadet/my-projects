public class SniperRifle {

    private int bulletDamage;

    public SniperRifle(int bulletDamage) {
        this.bulletDamage = bulletDamage;
    }

    public void shoot(Enemy enemy) {
        //Receive an enemy as parameter
        if (enemy.getHealth() <= 0) {
            enemy.setDead(true);
        } else {
            enemy.hit(bulletDamage);
        }
    }

    /*public int getBulletDamage() {
        return bulletDamage;
    }*/
}
