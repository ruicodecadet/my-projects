abstract public class Enemy extends GameObject{

    private int health;
    private boolean isDead;

    public boolean isDead(){
        return this.isDead;
    }

    public void hit(int damage) {
        health -= damage;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void setDead(boolean dead) {
        isDead = dead;
    }

    @Override
    public String getMessage() {
        return "I'm hit... ohhhh";
    }
}
