public class Tree extends GameObject{

    public Tree() {
        super();
    }

    @Override
    public String getMessage() {
        return "I'm a poor lonely tree, please don't shoot!";
    }
}
