public class ArmouredEnemy extends Enemy{

    private int armour;

    public ArmouredEnemy() {
        super();
        //System.out.println(getClass());
    }

    public void setArmour(int armour) {
        this.armour = armour;
    }

    @Override
    public String getMessage() {
        return "I'm a armoured soldier and i'm hit... ohhhh";
    }
}
