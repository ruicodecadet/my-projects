public class Game {

    private static final int NUM_OF_OBJECTS = 10;
    private GameObject[] gameObject;
    private SniperRifle sniperRifle;
    private int shotsFired;
    private int numOfTree, numOfSoldierEnemy;
    private ArmouredEnemy soldierArmour;
    private Enemy soldierHealth;

    public Game() {

        gameObject = new GameObject[NUM_OF_OBJECTS];
        sniperRifle = new SniperRifle(1);
        soldierArmour = new ArmouredEnemy();

    }

    public void start() {

        shotsFired = 0;
        soldierArmour.setArmour(10);

        soldierHealth.setHealth(5);

        for(GameObject enemy : gameObject) {

            if(enemy instanceof Tree){
                enemy.getMessage();
            } else {
                //Downcast of game object to enemy
                Enemy otherEnemy = (Enemy) enemy;
                while(!otherEnemy.isDead()) {
                    sniperRifle.shoot(otherEnemy);
                    shotsFired++;
                }
                System.out.println(otherEnemy.getMessage());
                System.out.println(shotsFired++);
            }
        }
    }

    public GameObject[] createObjects() {
        //Returns an GameObject[] and has an int parameter
        double random = Math.random();

        if(random > 0.4) {
            numOfTree = (int) (0.4 * NUM_OF_OBJECTS);
            //System.out.println(numOfTree + " Trees");
        } else {
            numOfTree = (int) Math.ceil(random * NUM_OF_OBJECTS);
            //System.out.println(numOfTree + " Trees");
        }

        numOfSoldierEnemy = (int) Math.ceil((NUM_OF_OBJECTS - numOfTree)/2);
        //System.out.println(numOfSoldierEnemy + " soldiers");

        for(int i = 0; i < gameObject.length; i++) {

            if(i < numOfTree){
                gameObject[i] = new Tree();
            }

            if(i >= numOfTree && i < (numOfTree + numOfSoldierEnemy)) {
                gameObject[i] = new SoldierEnemy();
            }

            if(i >= numOfTree + numOfSoldierEnemy) {
                gameObject[i] = new ArmouredEnemy();
            }
        }

        return gameObject;
    }
}
