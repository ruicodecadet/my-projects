package org.academiadecodigo.networking.server;

import java.io.BufferedReader;
import java.io.IOException;

public class ClientHandler {

    private BufferedReader inputBufferedReader;

    public ClientHandler() {


    }

    private void handleClient() {

        try {

            // handle client connection
            while (true) {


                // read line from socket input reader
                String line = inputBufferedReader.readLine();

                // if received /quit close break out of the reading loop
                if (line == null || line.equals("/quit")) {

                    System.out.println("Client closed, exiting");
                    break;

                }

                // show the received line to the console
                System.out.println(line);
            }


        } catch (IOException ex) {

            System.out.println("Receiving error: " + ex.getMessage());

        } finally {
            try {
                inputBufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}



